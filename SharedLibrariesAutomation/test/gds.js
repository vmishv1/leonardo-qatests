var GridDataService = require('libs-grid-data-service');
var fs = require('fs');
var path = require('path');
const assert = require('assert');
const chai = require('chai');
const mocha = require('mocha');

let obj = new GridDataService.GridDataService();

var image_file = path.join('testdata/gds', 'word_corrupt.png');
var word_file = path.join('testdata/gds', 'word-file.docx');
var ods_file = path.join('testdata/gds', 'ods_file.ods');
var excel_file = path.join('testdata/gds', 'GDS_ref-file.xlsx');
var fill_holes_excel_file = path.join('testdata/gds', 'fill_holes.xlsx');
var JSON_ref_path = path.join('testdata/gds', 'ref-json.json');
let GridDataServiceLimits = GridDataService.GridDataServiceLimits;
let GridDataServiceProps = GridDataService.GridDataServiceProps;
let GridDataServiceEntities = GridDataService.GridDataServiceEntities;

var fileIterator = 0
var fileArray = [image_file, word_file, ods_file]

console.log(obj.latestVersion()); // Validating the latest version of GDS schema

describe('Checking Error Codes (Negative Scenarios for uploading Excel)', () => {
    beforeEach(() => {
        return new Promise((resolve) => {
            obj.loadExcel(fs.readFileSync(fileArray[fileIterator++]))
                .then((gridModel) => {
                    reject();
                })
                .catch((err) => {
                    res = err.errorCode;
                    resolve();
                });
        })
    });

    it("should give Error E003", () => {
        chai.assert.equal(res, "E003");
    });
    it("should give Error E001", () => {
        chai.assert.equal(res, "E001");
    });
    it("should give Error E002", () => {
        chai.assert.equal(res, "E002");
    });
    fileIterator = 0;
});

describe("toJSON Validation", () => {
    var gridJson;
    beforeEach(() => {
        return new Promise((resolve, reject) => {
            obj.loadExcel(fs.readFileSync(excel_file)).then((gridModel) => {
                gridJson = gridModel.toJson(); // returns the latest version Grid JSON
                resolve();
            }).catch((err)=>{
                reject(err);
            })
    });
    });
    it("Requesting latest version Grid JSON", () => {
                chai.assert.isObject(gridJson);
            })
        // it("Requesting 0.1 Version Grid JSON",()=> { //Commenting for time being
        //     excelLoad.then((gridModel) => {
        //         var gridJson = gridModel.toJson("0.1");
        //         ref_JSON = JSON.parse(fs.readFileSync(JSON_ref_path));
        //         chai.assert.equal(JSON.stringify(gridJson), JSON.stringify(ref_JSON));
        //     })
        // })
});

describe("Load JSON Validations", () => {
    var gridModel;
    before(() => {
        return new Promise((resolve, reject) => {
            obj.loadJson(fs.readFileSync(JSON_ref_path)).then(
                (gm1) => {
                    gridJson = gm1.toJson();
                    resolve();
                }
            ).catch((err) => {
                reject(err);
            });
        });
    });
    // it('Checking JSON after deleting data object', () => {
    //     return new Promise((resolve,reject) => {
    //         error = false;
    //         excelLoad.then((gm) => {
    //             gridJson = gm.toJson();
    //             delete gridJson['data'];
    //             resolve();
    //         })
    //         .catch((err)=> {
    //             console.log(err);
    //             reject();
    //     });
    //     chai.assert.equal(true, error);
    //     })
    // });
    // it('Checking JSON after deleting meta object', () => {
    //     var res = false;
    //         excelLoad.then((gm1)=> {
    //             gridJson1 = gm1.toJson();
    //         })
    //         .then(()=> {
    //             delete gridJson1['meta'];
    //             gm3 = obj.loadJson(gridJson1);
    //         })
    //         .catch((err)=> {
    //             if(err){
    //                 res = true;
    //             }
    //     });
    //     chai.assert.equal(true, true);
    // });
    it('loading a valid JSON', () => {
        chai.assert.hasAllKeys(gridModel.toJson(), ['@schemaVersion','#sheets','activeSheet','defaults','names'], 'This is not a valid o/p JSON'); // validating the presence of data and meta node in o/p JSON
    })
});

let gridModel;
// Using GridDataServiceLimits
let limits = {
    MAX_ROWS: {
        DEFAULT: 200,
        LESS: 100,
        MORE: 250
    },
    MAX_COLUMNS: {
        DEFAULT: 50,
        LESS: 20,
        MORE: 60
    },
    MAX_SHEETS: {
        DEFAULT: 5,
        LESS: 3,
        MORE: 10
    }
}

describe("Using GridDataServiceLimits", () => {
    let flag = true;
    beforeEach(() => {
        if(flag){
            flag = false;
            return new Promise(
                (resolve, reject) => {
                    // console.log("in before each");
                    obj.loadExcel(fs.readFileSync(excel_file)).then((gridModelResEx) => {
                        gridModel = obj.loadJson(gridModelResEx.toJson());
                        gridJson_jsonInput = gridModel.toJson();
                        resolve();
                    }).catch(err => {
                        console.log(err);
                        reject();
                    });
            });
        }
    })
    afterEach(() => {
        GridDataServiceLimits.MAX_SHEETS = limits.MAX_SHEETS.DEFAULT;
        GridDataServiceLimits.MAX_ROWS = limits.MAX_ROWS.DEFAULT;
        GridDataServiceLimits.MAX_COLUMNS = limits.MAX_COLUMNS.DEFAULT;
    });
    it('Accessing cell inside the defaults limits', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][55]["#cells"][30].value, "AE56 cell");
    });

    it('Accessing cell outside MAX_ROWS row (when defaults limits are applied)', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][201], undefined); //202 row
    });

    it('Accessing cell outside MAX_COLUMNS column(when defaults limits are applied) ', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][1]["#cells"][51], undefined); //AZ2
    });

    it('Accessing cell outside MAX_SHEETS sheet (when defaults limits are applied)', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][5], undefined);
    });

    it('Modifying MAX_ROWS Limit (to limit larger than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_ROWS = limits.MAX_ROWS.MORE;
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][247], undefined);
    });

    it('Modifying MAX_COLUMNS Limit (to limit larger than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_COLUMNS = limits.MAX_COLUMNS.MORE;
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][0]["#cells"][52], undefined); //BA1
    });

    it('Modifying MAX_SHEETS Limit (to limit larger than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_SHEETS = limits.MAX_SHEETS.MORE;
        flag = true;
        chai.assert.equal(gridJson_jsonInput["#sheets"][6], undefined);
    });

    it('Modifying MAX_ROWS Limit (to limit smaller than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_ROWS = limits.MAX_ROWS.LESS;
        flag = true;
        // console.log(`Received: ${JSON.stringify(gridJson_jsonInput["#sheets"][0]["#rows"][149]["#cells"][0])} \nlimts: ${JSON.stringify(GridDataServiceLimits)}`);
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][149]["#cells"][0], undefined);
    });

    it('Modifying MAX_COLUMNS Limit (to limit smaller than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_COLUMNS = limits.MAX_COLUMNS.LESS;
        flag = true;
        // console.log(`Received: ${JSON.stringify(gridJson_jsonInput["#sheets"][0]["#rows"][0]["#cells"][24])} \nlimts: ${JSON.stringify(GridDataServiceLimits)}`);
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][0]["#cells"][24], undefined);
    });

    it('Modifying MAX_SHEETS Limit (to limit smaller than the default limit) and then accessing Cell Data between the old and new limits', () => {
        GridDataServiceLimits.MAX_SHEETS = limits.MAX_SHEETS.LESS;
        // console.log(`Received: ${JSON.stringify(gridJson_jsonInput["#sheets"][3])} \nlimts: ${JSON.stringify(GridDataServiceLimits)}`);
        chai.assert.equal(gridJson_jsonInput["#sheets"][3], undefined);
    });

    it('(Boundary Value Analysis) Accessing data from the last row, column for default limits', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][4]["#rows"][199]["#cells"][49].value, "AX200 Cell");
    });

});

describe('Testing GDS Limits using custom JSON Path (Excel)', () => {
    obj.loadExcel(fs.readFileSync(excel_file))
        .then((gm_new) => {
            gridJson_new = gm_new.toJson();
            console.log(gridJson_new.data["#sheets"][0]["#rows"][39]["#cells"][0].value); //AE56
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][0]["#cells"][1].value); // Y1
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][1]["#cells"][1].value); // AZ2
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][0]["#cells"][2].value); // BA1
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][41]["#cells"][0].value); // A150     
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][42]["#cells"][0].value); // AX200   
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][43]["#cells"][0].value); // A202   
            console.log(gridJson_new.data["#sheets"]["0"]["#rows"][44]["#cells"][0].value); // A248   
        })
});

// describe('Testing GDS Limits using custom JSON Path (JSON)',() => {
//     gm_json = obj.loadJson(JSON.parse(fs.readFileSync(JSON_ref_path)));
//     gridJson_jsonInput = gm_json.toJson();
//         console.log(gridJson_jsonInput.data["#sheets"][0]["#rows"][39]["#cells"][0].value); //AE56
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][0]["#cells"][1].value);// Y1
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][1]["#cells"][1].value);// AZ2
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][0]["#cells"][2].value);// BA1
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][41]["#cells"][0].value);// A150     
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][42]["#cells"][0].value);// AX200   
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][43]["#cells"][0].value);// A202   
//         console.log(gridJson_jsonInput.data["#sheets"]["0"]["#rows"][44]["#cells"][0].value);// A248   
// });

describe("Validation for section 4", () => {
    before(() => {
        // LoadExcel api

        return new Promise(
            (resolve, reject) => {
                obj.loadExcel(fs.readFileSync(excel_file)).then((gridModelResEx) => {
                    gridModel = obj.loadJson(gridModelResEx.toJson());
                    resolve();
                }).catch(err => {
                    console.log(err);
                    reject();
                });
            });
    })

    it("Accessing value at cell with value with getPropsByJsonPath", () => {
        let returnedValue = gridModel.getPropsByJsonPath("$['#sheets']['0']['#rows']['3']['#cells']['3']", GridDataServiceProps.CELL.VALUE);
        chai.assert.equal(returnedValue, "test");
    });

    it("Accessing value at cell without value with getPropsByJsonPath", () => {
        let returnedValue = gridModel.getPropsByJsonPath("$['#sheets']['0']['#rows']['8']['#cells']['2']", GridDataServiceProps.CELL.VALUE);
        chai.assert.equal(returnedValue, "");
    });

    it('Accessing value at cell with getProps at cell level', () => {
        let returnedValue = gridModel.getProps(GridDataServiceProps.CELL.VALUE, { sheetIndex: 0, cellRef: "AE56" });
        chai.assert.equal(returnedValue, "AE56 cell");
    });

    it('Accessing value at cell with getProps at row level', () => {
        let returnedValue = gridModel.getProps(GridDataServiceProps.ROW.CELL_COUNT, { sheetIndex: 0, rowIndex: 1 });
        chai.assert.equal(returnedValue, 1);
    });

    it('Accessing value at cell with getProps at column level A', () => {
        let returnedValue = gridModel.getProps(GridDataServiceProps.COLUMN.WIDTH, { sheetIndex: 0, columnIndex: 1 });
        chai.assert.equal(returnedValue, 220);
    });

    it('Accessing value at cell with getProps at column level B', () => {
        let returnedValue = gridModel.getProps(GridDataServiceProps.COLUMN.WIDTH, { sheetIndex: 0, columnIndex: 2 });
        chai.assert.equal(returnedValue, 64);
    });

    it('Accessing value at cell with getProps at sheet level', () => {
        let returnedValue = gridModel.getProps(GridDataServiceProps.SHEET.NAME, { sheetIndex: 0 });
        chai.assert.equal(returnedValue, "data-and-alignment");
    });

    it('addExtraInfo and getExtraInfo', () => {
        gridModel.addExtraInfo("newProp", { id: "alfjsbfahf1124" }, { sheetIndex: 0, cellRef: "A1" }, GridDataServiceEntities.CELL);
        let returnedValue = JSON.stringify(gridModel.getExtraInfo("newProp", { sheetIndex: 0, cellRef: "A1" }, GridDataServiceEntities.CELL));
        chai.assert.equal(returnedValue, JSON.stringify({ id: "alfjsbfahf1124" }));
    });

    it('using isPhysical at sheet level', () => {
        let returnedValue = gridModel.isPhysical(GridDataServiceEntities.SHEET, { sheetIndex: 0 });
        chai.assert.equal(returnedValue, true);
    });

    it('using isPhysical at row level', () => {
        let returnedValue = gridModel.isPhysical(GridDataServiceEntities.ROW, { sheetIndex: 0, rowIndex: 3 });
        chai.assert.equal(returnedValue, true);
    });

    it('using isPhysical at column level', () => {
        let returnedValue = gridModel.isPhysical(GridDataServiceEntities.COLUMN, { sheetIndex: 0, columnIndex: 1 });
        chai.assert.equal(returnedValue, true);
    });

    it('using isPhysical at cell level', () => {
        let returnedValue = gridModel.isPhysical(GridDataServiceEntities.CELL, { sheetIndex: 0, cellRef: "AE56" });
        chai.assert.equal(returnedValue, true);
    });

    it('using addValidation and getValidationById', () => {
        let validationNode = {
            "validate": "value",
            "operator": "equals",
            "extrainfo": {
                "ignoreCase": true,
                "trimWhitespace": true
            },
            "id": "R1.1"
        };
        let validationId = validationNode.id;
        let returnedValue = gridModel.getValidationById(validationId, { sheetIndex: 0, cellRef: "AE56" }, GridDataServiceEntities.CELL);
        chai.assert.equal(returnedValue, null);
        gridModel.addValidation(validationNode, { sheetIndex: 0, cellRef: "AE56" }, GridDataServiceEntities.CELL);
        returnedValue = gridModel.getValidationById(validationId, { sheetIndex: 0, cellRef: "AE56" }, GridDataServiceEntities.CELL);
        chai.assert.deepEqual(returnedValue, validationNode);
    });

});


describe('Testing GDS Limits using custom JSON Path (JSON)', () => {
    A = { data: "hi hello" };
    delete A["data"];
    console.log(A);

});

describe('Testing GDS fill holes functionality', () => {
    before(() => {
        // LoadExcel api
        return new Promise(
            (resolve, reject) => {
                obj.loadExcel(fs.readFileSync(fill_holes_excel_file)).then((gridModelResEx) => {
                    gridModel = obj.loadJson(gridModelResEx.toJson());
                    gridJson_jsonInput = gridModel.toJson();
                    resolve();
                }).catch(err => {
                    console.log(err);
                    reject();
                });
            });
    })

    it('Accessing cell with data in a row(using expected json path)', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][0]["#cells"][4].value, "hah");
    });

    it('Accessing cell with no data, after the last cell with data', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][1]["#cells"][4], undefined);
    });

    it('Accessing cell in a row with no data', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][5]["#cells"][1], undefined);
    });

    it('Accessing cell with no data, present before the last data cell in the row', () => {
        chai.assert.isObject(gridJson_jsonInput["#sheets"][0]["#rows"][6]["#cells"][2])
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][6]["#cells"][2].value, undefined);
    });

    it('Accessing cell(with no data) present vertically between two data filled cells( same column ) in a row with no data cells. ', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][5]["#cells"][3], undefined);
    });

    it('Accessing cell in a row with the only data cell at cell outside the GDS limits', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][8], undefined);
    });

    it('Applying formatting on a cell and accessing data on a cell after it', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][4]["#cells"][2], undefined);
    });

    it('Applying formatting on a cell and accessing data on a cell before it', () => {
        chai.assert.isObject(gridJson_jsonInput["#sheets"][0]["#rows"][4]["#cells"][0]);
        chai.assert.equal(gridJson_jsonInput["#sheets"][0]["#rows"][4]["#cells"][0].value, undefined);
    });

    it('Merging two cells vertically and accessing cell before the first cell', () => {
        chai.assert.isObject(gridJson_jsonInput["#sheets"][1]["#rows"][5]["#cells"][11]);
        chai.assert.equal(gridJson_jsonInput["#sheets"][1]["#rows"][5]["#cells"][11].value, undefined);
    });

    it('Merging two cells vertically and accessing cell before the last cell', () => {
        // console.log(JSON.stringify(gridJson_jsonInput["#sheets"][1]["#rows"][6]["#cells"][11]));
        chai.assert.equal(gridJson_jsonInput["#sheets"][1]["#rows"][6]["#cells"][11], undefined);
    });

    it('Merging two cells vertically and accessing cell after the first cell', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][1]["#rows"][5]["#cells"][13], undefined);
    });

    it('Merging two cells vertically and accessing cell after the last cell', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][1]["#rows"][6]["#cells"][13], undefined);
    });

    it('Leaving first row blank and accessing 2nd row\'s data using json path with 1 index row', () => {
        chai.assert.equal(gridJson_jsonInput["#sheets"][1]["#rows"][1]["#cells"][1].value, "B2 cell");
    });
});