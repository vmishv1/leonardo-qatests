var assert = require('chai').assert;
const winston = require('../Log.js');
const fs = require('fs');
argv = require('minimist')(process.argv.slice(1));
var gVersion = argv.gversion
var validateMatcherOutput = require('../utils/custom-functions/validateMatcherOutput.js')
var validateGraderOutput = require('../utils/custom-functions/validateGraderOutput.js')
var solutionJSON, solutionJSON, expectedJSON, expectedJSON, expectedJSON
function test(item, submission, func, done) {
  try {
    if (func.toLowerCase().trim() == "matcher") {
      solutionJSON = fs.readFileSync(item + "/Solution.json")
      settingsJSON = fs.readFileSync(item + "/settingsJSON.json")
      submissionJSON = fs.readFileSync(submission + "/Submission.json")
      expectedJSON = fs.readFileSync(submission + "/Expected.json")
      var matcherOutput = validateMatcherOutput(solutionJSON, SubmissionJSON, settingsJSON, expectedJSON)
      if (matcherOutput == "")
        done()
      else
        done(new Error(compareOutput));
    }
  }
  catch (err) {
    if (err.code === 'ENOENT') {
      done(new Error('File not found!'));
    } else {
      throw err;
    }
  }
  try {
    if (func.toLowerCase().trim() == "grader") {
      solutionJSON = fs.readFileSync(item + "/Solution.json")
      scoringJSON = fs.readFileSync(item + "/Scoring.json")
      settingsJSON = fs.readFileSync(item + "/settingsJSON.json")
      submissionJSON = fs.readFileSync(submission + "/Submission.json")
      expectedJSON = fs.readFileSync(submission + "/Expected.json")
      var graderOutput = validateGraderOutput(solutionJSON, submissionJSON, scoringJSON, settingsJSON, expectedJSON);
      if (graderOutput == "") {
        done()
      }
      else {
        done(new Error("Scoring are not matched for " + graderOutput));
      }
    }
  }
  catch (err) {
    if (err.code === 'ENOENT') {
      done(new Error('File not found!'));
    } else {
      throw err;
    }
  }
}
describe("Grader Test", function () {
  try {

    var testRepoString = fs.readFileSync('./itemMaster/TestRepositary.json');
    var testrepoJSON = JSON.parse(testRepoString);
    var graderVersions = gVersion.toString().split(",");
    for (j = 0; j < graderVersions.length; j++) {
      var graderVersionNumber = graderVersions[j];
      var graderversion = testrepoJSON.find(grader => grader.version === graderVersionNumber);

      describe("Grader Version: " + graderVersionNumber + " Testcase ", function () {
        this.timeout(99999);
        var testDetails = graderversion.testDetails;
        for (var i = 0; i < testDetails.length; i++) {
          var testSuite = testDetails[i]
          var testCasePath = testSuite.testCasePath
          var testCaseIds = testSuite.testCaseIds
          var testCaseDetailsString = fs.readFileSync('./testdata/grader/' + testCasePath)
          var testCaseDetailsJson = JSON.parse(testCaseDetailsString)
          describe(testSuite.testCaseType + " Testcases", function (client) {

            for (var j = 0; j < testCaseIds.length; j++) {
              var id = testCaseIds[j];
              var testCase = testCaseDetailsJson.find(testCase => testCase.TCID === id)
              var testCaseDescription = testCase.testCaseDescription
              var testCaseTags = testCase.tags
              const assignment = testCase.item
              const submission = testCase.submission
              const func = testCase.function
              it(testCase.TCID + ':' + testCaseDescription + '--' + testCaseTags, function (done) {
                test(assignment, submission, func, done, function () { console.log(testCaseDescription + " end") });
              })
            }
          });
        }
      })
    }
  }
  catch (err) {
    console.log(err)
    logger.error(graderVersions + " is not available in Item repositary")
  }
});
