const fs = require("fs");
const chai = require('chai');
const gdsLimitsTest = require('../utils/custom-functions/gdsLimitsTest.js');
const jsonToJson = require('../utils/custom-functions/jsonToJson.js');
const excelToJson = require('../utils/custom-functions/excelToJson.js');
const gdsVersionTest = require('../utils/custom-functions/gdsVersionTest');
const isPhysicalTest = require('../utils/custom-functions/isPhysicalTest');
var gVersion = "1";


describe("LEONARDO BACK-END TESTCASES", function() {
  var testRepoString = fs.readFileSync('./itemMaster/TestRepository_gds.json');
  var testrepoJSON = JSON.parse(testRepoString);
  var graderVersions = gVersion.toString().split(",");
  for (j = 0; j < graderVersions.length; j++) {
    var graderVersionNumber = graderVersions[j];
    var graderversion = testrepoJSON.find(grader => grader.version === graderVersionNumber);

    describe("GDS Test Version: " + graderVersionNumber + " Testcase ", function () {
      var testDetails = graderversion.testDetails;
      for (var i = 0; i < testDetails.length; i++) {
        var testSuite = testDetails[i]
        var testCasePath = testSuite.testCasePath
        var testCaseIds = testSuite.testCaseIds
        var testCaseDetailsString = fs.readFileSync('./testdata/gds/' + testCasePath)
        var testCaseDetailsJson = JSON.parse(testCaseDetailsString)
        describe(testSuite.testCaseType + " Testcases", function () {

          for (var j = 0; j < testCaseIds.length; j++) {
            var id = testCaseIds[j];
            var testCase = testCaseDetailsJson.find(testCase => testCase.TCID === id)
            var testCaseDescription = testCase.TestCase
            var testCaseTags = testCase.Tags
            let TC_function = testCase.FunctionName;
            let allArgs = Object.keys(testCase);
            allArgs.splice(0,4);
            let funcArgsVal = [];
            allArgs.forEach((index)=> {
              funcArgsVal.push(testCase[index]);
          });
            it(testCase.TCID + ':' + testCaseDescription +  ' -- Tags: ' + testCaseTags, async function () {
              await eval(TC_function)(...funcArgsVal);
            })
          }
        });
      }
    })
  }
});