const fs = require("fs");
const GridDataService = require('libs-grid-data-service');
const chai = require('chai');
const jsonToJson = require('./jsonToJson');
const excelToJson = require('./excelToJson');
const cellInformation = require('./cellInfo');
const validate = require('./validateGDS')

let gdsObject = new GridDataService.GridDataService(); // instantiating a new object of GDS
module.exports = async function gdsLimitsTest(lim, inp, workSheetName, cellAddr, expectedVal) {

    let GridDataServiceLimits = GridDataService.GridDataServiceLimits;
    let output_gridJSON;
    
    GridDataServiceLimits.MAX_SHEETS = lim.MAX_SHEETS ? lim.MAX_SHEETS: 5
    GridDataServiceLimits.MAX_ROWS = lim.MAX_ROWS ? lim.MAX_ROWS: 200,
    GridDataServiceLimits.MAX_COLUMNS = lim.MAX_COLUMNS ? lim.MAX_COLUMNS: 50

    if(inp.split('.')[1] == 'xlsx'){
        let excelToJsonOP = await excelToJson(inp,"","0.1"); 
        let JsonToJsonOP = await jsonToJson(excelToJsonOP,"", "0.1","json");     
        output_gridJSON = JsonToJsonOP;
    }
        else if(inp.split('.')[1] == 'json') {
            output_gridJSON = await jsonToJson(inp,"", "0.1","file"); 
        }

        if(output_gridJSON) {
            let cellInfo = cellInformation(output_gridJSON,workSheetName,cellAddr);    
            let sheetNum = cellInfo.sheetNum;
            let rowNum = cellInfo.rowNum;
            let colNum = cellInfo.colNum;
            let actualVal;
            if(sheetNum < GridDataServiceLimits.MAX_SHEETS){
                if(rowNum < GridDataServiceLimits.MAX_ROWS){
                    if(colNum < GridDataServiceLimits.MAX_COLUMNS){
                        actualVal = cellInfo.value;
                    }else{
                        actualVal = "undefined_cell";
                    }
                }else{
                    actualVal = "undefined_row";
                }
            }else{
                actualVal = "undefined_sheet";
            }
        
            //Calling validation function now...
            validate('value',actualVal,expectedVal);
        }
        else
            console.log('ERROR: Error in computing Grid JSON!');
    }
