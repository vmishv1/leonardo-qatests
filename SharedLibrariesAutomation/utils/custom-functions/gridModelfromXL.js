const GridDataService = require('libs-grid-data-service');
const fs = require('fs');

let obj = new GridDataService.GridDataService();

module.exports = function(inputExcel){
    return new Promise((resolve, reject) => {
        
        obj.loadExcel(fs.readFileSync('./testdata/gds/' + inputExcel))
        .then((gm) => {
            gridJson = gm.toJson();
            gm2 = obj.loadJson(gridJson);
            resolve(gm2);
        }).catch(err => {
            reject(err);
        })
    })
}