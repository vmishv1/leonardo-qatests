const GridDataService = require('libs-grid-data-service');
const validate = require('./validateGDS');
const getGridModelfromXL = require('./gridModelfromXL')
let GridDataServiceEntities = GridDataService.GridDataServiceEntities;

module.exports = async function isPhysicalTest(inputFile, propName, entityReference,expected){


    // let returnedValue = gridModel.isPhysical(GridDataServiceEntities.SHEET, { sheetIndex: 0 });
    // chai.assert.equal(returnedValue, true);
    let gm =  await getGridModelfromXL(inputFile);
    let actualVal = gm.isPhysical(GridDataServiceEntities[propName],entityReference);

    validate('value',expected,actualVal);

}