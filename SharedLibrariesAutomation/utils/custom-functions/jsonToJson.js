const fs = require('fs');
const GridDataService = require('libs-grid-data-service');
const chai = require('chai');
const validate = require('./validateGDS');
let gdsObject = new GridDataService.GridDataService(); // instantiating a new object of GDS
module.exports = async function jsonToJson (inp, referenceFile, gdsVer = null, inputType = 'file') {
    return new Promise((resolve) => {
        gdsVer = gdsVer || null;

        if(inputType == 'file'){
            gridModel2 =gdsObject.loadJson(JSON.parse(fs.readFileSync('./testdata/gds/' + inp)));
        }
        else if(inputType == 'json'){
            gridModel2 =gdsObject.loadJson(inp);
        }
        gridJson = gridModel2.toJson(gdsVer); //always fetching the latest version of JSON
        if(referenceFile != ""){
            validate("JSON",referenceFile, gridJson);
        }
        resolve(gridJson);
        return gridJson;
    
    })
}