const fs = require('fs');
const GridDataService = require('libs-grid-data-service');
const chai = require('chai');
const validate = require('./validateGDS');

let gdsObject = new GridDataService.GridDataService(); // instantiating a new object of GDS
module.exports = function excelToJson (inp, referenceFile, gdsVersion) { 
    return new Promise((resolve,reject)=> {
        var refFlag;
    gdsObject.loadExcel(fs.readFileSync('./testdata/gds/' + inp))
        .then((gridModel) => {
            gridJson = gridModel.toJson(gdsVersion);
            refFlag = referenceFile.split('.')[1] == 'json' ? true : false
            if(refFlag) {
                validate('JSON', referenceFile, gridJson);    
            }
            resolve(gridJson);
        })
        .catch((err) => {
            if(refFlag){
                reject("JSON didn't match");
            }
            else {
                if(referenceFile){
                    validate('value',referenceFile,err.errorCode);
                    resolve(err.errorCode);    
                }
                else {
                    console.log(err);
                }
            }
        });
    })
}