module.exports = function cellInfo(gridJson, workSheetName, cellAddr) {
 
    //gridJson_jsonInput["#sheets"][1]["#rows"][5]["#cells"][11].value
    let totalSheetCount = gridJson["#sheets"].length;
    let sheetIndex;

    for (let sheetIndexIterator = 0; sheetIndexIterator < totalSheetCount; sheetIndexIterator++) {
        if(gridJson["#sheets"][sheetIndexIterator].name == workSheetName) {
            sheetIndex = sheetIndexIterator;
            break;
        }
    }
    
    let colAddr = cellAddr.match(/[A-Z]+/g)[0];
    let rowNum = cellAddr.replace(/[^0-9]/g, '') - 1;
    let colNum = (colAddr.length - 1)*26*(colAddr.charCodeAt(0) - 64) + colAddr.charCodeAt(colAddr.length - 1) - 65
    let actualVal;
    let cellNode;
    try{
        cellNode = gridJson["#sheets"][sheetIndex]? (gridJson["#sheets"][sheetIndex]["#rows"][rowNum]? (gridJson["#sheets"][sheetIndex]["#rows"][rowNum]["#cells"][colNum]) : ({"value":"undefined_row"})):({"value":"undefined_sheet"});
        actualVal = cellNode? (cellNode.value? cellNode.value: "undefined"): "undefined_cell";    
    }
    catch(err){
        actualVal = undefined;
    }

    return {
        "value":actualVal,
        "sheetNum":sheetIndex,
        "rowNum": rowNum,
        "colNum": colNum
    };
}