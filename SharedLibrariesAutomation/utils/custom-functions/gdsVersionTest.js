const GridDataService = require('libs-grid-data-service');
const validate = require('./validateGDS');

module.exports = function gdsversionTest(testType,expected){

    let obj = new GridDataService.GridDataService();

    if(testType == 'latestVersion'){
        validate('value',expected,obj.latestVersion());
    }
    else if(testType =='availableVersion'){
        validate('array-comparison',expected,obj.availableVersions());
    }
}