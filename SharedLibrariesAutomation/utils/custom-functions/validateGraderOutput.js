const Grader = require('libs-leonardo-grader/dist/leonardograder.js');
const compareJSON = require('./jsonCompare.js');
const fs = require('fs');

module.exports = function validateGraderOutput(solutionJSON, submissionJSON, scoringJSON, settingsJSON,expectedJSON) {
	try{
				var graderObject = new Grader.LeonardoGrader();
				var solutionJSON1= JSON.parse(solutionJSON);
				var submissionJSON1= JSON.parse(submissionJSON);
				var scoringJSON1= JSON.parse(scoringJSON);
			    var settingsJSON1= JSON.parse(settingsJSON);
				var expectedJSON1= JSON.parse(expectedJSON);
			

						var output = graderObject.Grade(solutionJSON1, submissionJSON1, scoringJSON1, settingsJSON1);
						var  ScorerJSON=JSON.stringify(output);
						var scoredJSONOutput = output.scoredJson
						var expectedJSON2=expectedJSON1.scoredJson

						var  expectedJSONOutput1=JSON.stringify(expectedJSON2);
						var  ScoredJson=JSON.stringify(scoredJSONOutput);
						//Expected Data json fetch
					
					
						fs.writeFile("../SharedLibrariesAutomation/output/Expected.json", ScorerJSON, (error) => { /* handle error */ });
						fs.writeFile("../SharedLibrariesAutomation/output/expectedJSON1.json", expectedJSONOutput1, (error) => { /* handle error */ });
						
						
							
				  	var difference = compareJSON(scoredJSONOutput, expectedJSON2)
					fs.writeFile("../SharedLibrariesAutomation/output/difference.json", difference, (error) => { /* handle error */ });
					return difference
		}

											
			catch(err){
           	console.log(err)
            logger.error("Grader function generate" +err)
           	 }
}
