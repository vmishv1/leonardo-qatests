//const chai = require('chai');
const fs = require('fs');
const compareJSON = require('./jsonCompare.js')
const deepEqualInAnyOrder = require('deep-equal-in-any-order');
const chai = require('chai');

chai.use(deepEqualInAnyOrder);

const { expect } = chai;

module.exports = function validate(validateType, expected, actual) {

    if(validateType == 'value') {
        chai.assert.equal(actual,expected);
    }
    else if (validateType == 'JSON') {
        
        let referenceJson = JSON.parse(fs.readFileSync('./testdata/gds/' + expected));
        let difference = compareJSON(referenceJson, actual);

        if(difference != false) {
            fs.writeFileSync('./testdata/json-comparison/diff.json', difference);  
        }
        else {
            chai.assert.isOk('all right');
        }
    }
    else if(validateType == 'array-comparison'){
        //chai.assert.equal(actual,expected);
        expect(actual).to.deep.equalInAnyOrder(expected);
    }
    
}