# Leonardo Grader Test Run

###  Getting Started:

The document contains the following information:

1. Steps to create the Leonardo Grader Test Cases
2. Steps to run the Grader tests in the QA Environment
3. Steps to run the Grader tests in the GIT Pipeline

[Test Case Link](https://docs.google.com/spreadsheets/d/1NT_WFhU4YRGygyF50RoX6Jlr-ehJax2ZbhRXpdiN3XU/edit#gid=1669077137)

### GIT Latest

- Clone the Repository

```
mkdir LeonardoGrader
cd LeonardoGrader
git clone https://bitbucket.org/comprodls/libs-leonardo-grader.git(Enter UserName & Password)
```

- Navigate the Grader Funtional test root folder (..libs-leonardo-grader\test\E2ETest)

### Test Case Creation Steps:

1. Extract the Solution, ScoringRules, Settings, Submissions, ExpectedRules JSON files.
2. Create a new folder (say Item1) under testData (..\test\E2ETest\testdata) folder. under the created folder create two new subfolder(Say Input, Submission) put Solution, ScoringRules, Settings json files in *Input Folder and Submissions, ExpectedRules JSON in *Submission folder.
3. Update the Test Cases information in the specific Testdata.json ( For e.g. MatcherTest\_OutofOrder.json ). Following are the key parameters that need to be updated:

	  - TCID: Unique ID of Test Case
	  - TestCase: Description of Test Case
	  - Tags: Test Case tag. It could be Priority, Type of test (Matcher, Grader etc)
	  - Assignment: Path of Item JSON Folder
	  - Submission: Path of Submission, Expected Rule JSON
	  - Function: Type of Test Operation (Grader: For complete Grading Test, Matcher: For Matching Results.
	  
Sample JSON (for reference)
```
{
"TCID":"MTC02",
"TestCase": "Sample",
"Tags": "P1, Matcher_OOO",
"Assignment":"./testdata/Item1/Input",
"Submission":"./testdata/Item1/MTC02",
"Function":"Grader"
}
```

2. Add the test case information in the Master Test Case Repository.json. Following are the key parameters that need to be updated:
	  - testcasePath: Test Case File Name (Test Case File Name will be available at the following location)
	  - TCID: Enter the test case id
	  
Sample Json
```
"Version_1": {
     "MatcherTest_OutOfOrder": {
        "testcasePath": "MatcherTest_OutofOrder",
        "TCID": "MTC02"
      },
```

# Test Execution Process:

**Option 1:** Test Execution on QA Environment ** **

**Manual Execution:**

```
Note: In case any new attribute is added in scoring json. User can update follwoing file to ignore that attribute:
File Path: utils\custom-functions\jsonCompare.js
command for ignoring attribute: Add (key === 'newattribute') in returtn statement of json/compare function
 
```
1. Install npm dependencies

```
npm install
```
2. Update the Package json for selecting the test cases and grader version (Optional)

```
"test": "mocha test/graderTest.js  --grep \"TC\" --reporter mochawesome --reporter-options overwrite=False --global Version_1 ",
"test1": "mocha test/graderTest.js  --grep \"MTC10|MTC02|MTC03|MTC01|MTCV102\" --reporter mochawesome --reporter-options overwrite=False  --global Version_1,Version_2 ",
```
3. Run the test

- To run default test using command prompt: 
```
npm test
```
- To run Specific test case using command prompt
```
"mocha test/graderTest.js  --grep \"MTC10|MTC02|MTC03|MTC01|MTCV102\" --reporter mochawesome --reporter-options overwrite=False  --global Version_1,Version_2
```
- To run test specific test cases when you updated package json specifying your test
```
npm run-script test1
```

Run Test of Multiple versions in one go **Execution using batch file:**

- Locate the batch file "GraderTest.bat" (default set on master branch)
- Update the build version for checkout (if required)
```
git.exe checkout -f    master(*Change Version*)
```
- Run the batch file.

**Option 2:** Test Execution through GIT Pipeline

- Once validated in test enviorment, Commit the test cases in the Grader repository. This will trigger the GIT Pipeline
- Once Unit test passed, Grader Functional test cases will be triggered through GIT pipeline
- Mail notification will be triggered to all Watcher in case test fails