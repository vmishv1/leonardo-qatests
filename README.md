# Leonardo Client Side Automation Framework 

### Description 

Leonardo Client Side Automation Framework is a node.js based framework using nightwatch and mocha runner. This is being used to automate the functional testcases for Leonardo Paint, Player, Item Renderer and Demo items.

## Setup Project on a local machine

To setup a project on your local machine, follow the mentioned steps:

- **Clone the Repository**
```
mkdir LeonardoAutomation
cd LeonardoAutomation
git clone https://bitbucket.org/vmishv1/leonardo-qatests.git(Enter UserName & Password)
```

- **Install the npm package after switching to the main folder**
```
cd leonardo-qatests\LeonardoAutomation
npm install
```

- **Run Default tests specified in nightwatch.json file**
```
npm test
```

## Test Apps
Following Test Apps are currently available under leonardo-qatests branch (./tests)

- Paint
- Player
- Item Renderer
- Demo

For Detailed Information, Refer to corresponding README.md File