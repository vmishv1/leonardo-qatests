# Running ItemRenderer Tests

To run ItemRenderer tests, the *appType* parameter needs to defined first. For e.g.
```
npm test -- --appType=ItemRenderer
```

To run Item Renderer test folder/files, **testFiles** parameter need to be passed. For e.g. The below command will run all the testfiles available inside the folder "ItemRenderer"
```
npm test -- --appType=ItemRenderer --testFiles="./tests/ItemRenderer"
```
Similarly, to run a specific testfile (say Hint Feature file), following command can be passed:
```
npm test -- --appType=ItemRenderer --testFiles="./tests/ItemRenderer/Default/hintFeatureTest.js"
```

## Run Time Parameters available for Item Renderer Tests:

- **testEnv** - To run tests in any environment (QA, Staging, Production or Testbench), use parameter **testEnv**. For e.g. The below command will run all the testcases of the files present inside the Item Renderer folder in the staging environment

```
npm test -- --appType=ItemRenderer --testFiles="./tests/ItemRenderer" --testEnv=staging
```
The testEnv parameter has following default values defined in the framework for the TestEnv parameter:
** QA
** Staging
** Production
** Testbench

### Parameters available while running Item Renderer Tests with Testbench

- **name** - The value passed using the "name" paramter will be set in the Name box in the Item Player Configuration area in the testbench

- **ver** - The value passed using the "ver" paramter will be set in the Version box in the Item Player Configuration area in the testbench

- **height** - The value passed using the "height" paramter will be set in the Height box in the Item Player Container Settings area in the testbench

- **width** - The value passed using the "width" paramter will be set in the Width box in the Item Player Container Settings area in the testbench

- **itemJSON** - The itemJSON parameter should have the path of the item json file that needs to be set in the testbench. Also, the path should be relative to the source folder.
For e.g. if any item json file is present at the location ./testdata/ItemJSON/Player/test1.json, following command needs to be executed
```
npm test -- --appType=ItemRenderer --testFiles="./tests/Player" --itemJSON="/testdata/ItemJSON/Player/test1.json" --name=leo-question-player --ver=0.4.1 --height=100% --width=80%
```

If nothing is passed, following default values will be set:

- Name - leo-question-player
- Version - Latest version available in the Testbench
- Container Height - 1600px
- Container Width - 700px
- Item JSON - Defined in tests file

Other than the above values, a **URL** can also be passed in the testEnv variable. It will directly launch that URL.

### Other Parameters available:

- **grep** - To run any specific testcase, **grep** command can be used. For e.g. The below command will run all the testcases inside the "ItemRenderer" folder which includes "TC02" in the test case name in the "qa" environment.

```
npm test -- --appType=ItemRenderer --testFiles="./tests/ItemRenderer" --testEnv=qa --grep=TC02
```

- **invert** - To run all testcases which does **NOT** include any specific word, use the **invert** parameter with **grep** command. For e.g. The below command will run all the Item Renderer testcases which does not include "Bug" in the test case name on the "qa" environment.

```
npm test -- --appType=ItemRenderer --testFiles="./tests/ItemRenderer" --testEnv=qa --grep=Bug --invert
```

## Default Run Commands already added in Package.json
For ease, following default commands are already present inside the package.json file
```
npm run itemRendererPlayerQA 
	--Run all testfiles present inside Item Renderer folder in QA environment using the Embed URL
npm run itemRendererPlayerStg
	--Run all testfiles present inside Item Renderer folder in Staging environment using the Embed URL 
npm run itemRendererPlayerProd 
	--Run all testfiles present inside Item Renderer folder in Production environment using the Embed URL 
npm run itemRendererTestbench 
	--Run all testfiles present inside Item Renderer folder with latest version available on Testbench and will run all testcases which does not include "Bug" inside the Testcase name 
```
