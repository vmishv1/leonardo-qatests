const fs = require('fs');
var rimraf = require('rimraf');

if (typeof argv.datafiles == 'string') {
    argv.datafiles = [argv.datafiles]
} else if (typeof argv.datafiles == 'undefined') {
    console.log("No Data file is mentioned. Running default data file i.e. leonardoSpreadsheet_testcases.json")
    argv.datafiles = ['leonardoSpreadsheet_testcases.json']
}

var j = 0;
var i;
var r = /\d+/;

var syncLoop = require(currentDirPath + '/utils/modules/syncLoop.js')

function performAction(result, iteration, precheckEvaluation, client) {

    try {

        if (result.Event.includes("selectCell")) {
            return new Promise(function(resolve, reject) {
                client.selectCell(result.Target, function() {
                    resolve(iteration + ". Action performed: Select Cell " + result.Target)
                })
            })
        }

        if (result.Event.includes("selectElement")) {
            return new Promise(function(resolve, reject) {
                client.selectElement(String(result.Target), function() {
                    resolve(iteration + ". Action performed: Select Element " + result.Target)
                })
            })
        };

        if (result.Event.includes("selectRange")) {
            return new Promise(function(resolve, reject) {
                client.selectRange(String(result.Target), function() {
                    resolve(iteration + ". Action performed: Select Range " + result.Target)
                })
            })
        };

        if (result.Event.includes("selectRows")) {
            return new Promise(function(resolve, reject) {
                client.selectRows(String(result.Target), function() {
                    resolve(iteration + ". Action performed: Select Rows " + result.Target)
                })
            })
        };

        if (result.Event.includes("selectColumns")) {
            return new Promise(function(resolve, reject) {
                client.selectColumns(String(result.Target), function() {
                    resolve(iteration + ". Action performed: Select Columns " + result.Target)
                })
            })
        };

        if (result.Event.includes("enterText")) {
            return new Promise(function(resolve, reject) {
                client.enterText(String(result.Target), result.ElementName, function() {
                    resolve(iteration + ". Action performed: Enter Text " + result.Target)
                })
            })
        };

        if (result.Event.includes("keyPress")) {
            return new Promise(function(resolve, reject) {
                client.keyPress(String(result.Target), function() {
                    resolve(iteration + ". Action performed: Press keys " + result.Target)
                })
            })
        };
		
        if (result.Event.includes("dragAndDrop")) {
            return new Promise(function(resolve, reject) {
                client.dragAndDrop( result.originElement,result.origin_x_offset,result.origin_y_offset,result.targetElement,result.target_x_offset,result.target_y_offset, precheckEvaluation, function() {
                    resolve(iteration + ". Action performed: Drag and drop " + result.originElement + " to " + result.targetElement)
                })
            })
        }

        if (result.Event.includes("validateCell")) {
            return new Promise(function(resolve, reject) {
                client.validateCell(result.ActiveCell, result.Target, result.Property, result.Expected, precheckEvaluation, function() {
                    resolve(iteration + ". Validation completed")
                })
            })
        }
		
        if (result.Event.includes("validateElement")) {
            return new Promise(function(resolve, reject) {
                client.validateElement(result.ElementName, result.Target, result.Property, result.Expected, precheckEvaluation, function() {
                    resolve(iteration + ". Validation completed")
                })
            })
        }
		
		if (result.Event.includes("validateTooltip")) {
            return new Promise(function(resolve, reject) {
                client.validateTooltip(result.ElementName, result.Expected, function() {
                    resolve(iteration + ". Action performed: Tooltip validated for " + result.ElementName)
                })
            })
        }
		
        if (result.Event.includes("movetoAnElement")) {
            return new Promise(function(resolve, reject) {
                //console.log("calling movetoAnElement")
                client.movetoAnElement(result.Element,result.x_offset,result.y_offset, precheckEvaluation, function() {
                    resolve(iteration + ". Action performed: Move to element " + result.Element)
                })
            })
        }
		
        if (result.Event.includes("doubleClickAtPosition")) {
            return new Promise(function(resolve, reject) {
                //console.log("performing double click")
                client.doubleClickAtPosition(result.Target, function() {
                    resolve(iteration + ". Action performed: Double click on " + result.Target)
                })
            })
        }
		
        if (result.Event.includes("validateBorder")) {
            return new Promise(function(resolve, reject) {
                //console.log("calling validateBorder")
                client.validateBorder(result.ElementName, result.Target, function() {
                    resolve(iteration + ". Validation of "+result.Target+ " is completed")
                })
            })
        }
		
		if (result.Event.includes("pauseForSomeTime")) {
            return new Promise(function(resolve, reject) {
                //console.log("calling pauseForSomeTime")
                client.pauseForSomeTime(result.Target, function() {
                    resolve(iteration + ". Action performed: Wait for "+result.Target+ " ms")
                })
            })
        }
		
        else {
			//console.log("!! ERROR !! Invalid function used !!")
            return new Promise(function(resolve, reject) {
                client.defaultFunctions(result.Event,result.Target,function(){
                    resolve(iteration + ". Default nightwatch function used "+ result.Event)
                })
            })
		}

    } catch (err) {
        console.log(err);
        client.end();
    }
};


function extendedValidation(extendedValidationCells, client) {

    cellsToValidate = extendedValidationCells.toString().split(",");

    return new Promise(function(resolve, reject) {

        if (cellsToValidate[0] != "") {
            console.log("Initializing Precheck")
            client.preCheck(cellsToValidate, function() {
                resolve(true);
            })

        } else {
            client.perform(function() {
                resolve(false);
            })
        }
    })
}


function testcaseExecution(cellsToValidate, actionIterator, AA, TC_SkipValidationProperty, client) {

    rimraf('./temp/*', function() { /*console.log("Deleting Files Before a Test"); */ })

    self = client;

    // var precheckEvaluation = await extendedValidation(cellsToValidate, self);

    //precheckEvaluation = false;
    var precheckEvaluation;

    cellsToValidateArray = cellsToValidate.toString().split(",");

    client.perform(function() {
        if (cellsToValidateArray[0] != "") {
            console.log("Initializing Precheck")

            client.preCheck(cellsToValidateArray, function() {
                precheckEvaluation = true;
            })

        } else {

            client.perform(function() {
                precheckEvaluation = false;
            })

        }

    })

    client.perform(function(callback) {
        syncLoop.loopExecution(actionIterator, function(loop) {
                var itr = loop.iteration();
                performAction(AA[itr], itr+1, precheckEvaluation, client).then((str) => {
                    console.log(str)    
					loop.next();
                })
            },
            function() {
                if (precheckEvaluation === true) {
                    console.log("Precheck Set to :" + precheckEvaluation);
                    // client.postCheck(cellsToValidateArray);
                    client.postCheck(cellsToValidateArray, function() {
                        client.compare(TC_SkipValidationProperty, function() {
                            callback(console.log("End"))
                        })
                    })

                } else {

                    rimraf('./temp/*', function() { /*console.log("Deleting Files when no comparison");*/ })
                        //console.log("Precheck Set to :" + precheckEvaluation)
                    //console.log("Test Case Status :");
                    console.log("***** End of Test Case *****");
                    callback(/*console.log("End")*/)
                }
            })
    })
}

describe("LEONARDO SPREADSHEET", function(client) {

    argv.datafiles.forEach(function(temp) {
        var rawdata = fs.readFileSync('./testdata/' + temp);
        data = JSON.parse(rawdata);
        Object.keys(data).forEach(function(Suitekey) {

            describe("Test Suite : " + data[Suitekey].Name, function(client) {
                this.timeout(99999999);

                before(function(client, done) {
                    var jsonData = String(data[Suitekey].Json).match(/\d+/);
                    client.launchURL("spreadsheetCase" + jsonData[0], client, done)
                    console.log("\n******************** Starting Test Suite " + data[Suitekey].Name + " ***************");
                })

                var testcase = data[Suitekey];
                const testCasesNum = Object.keys(data[Suitekey]).length;

                Object.keys(testcase).forEach(function(testCaseKey) {

                    //to skip name and Json keys
                    if (testCaseKey.includes("TC")) {
                        var TCID = testCaseKey;
                        var TC_NAME = data[Suitekey][testCaseKey].TestCase;
                        var TC_Tags = data[Suitekey][testCaseKey].Tags;
                        const TC_RequirePreCheck = data[Suitekey][testCaseKey].ExtendedValidation;
                        const TC_SkipValidationProperty = data[Suitekey][testCaseKey].SkipExtendedValProp;
                        const actionsToPerform = data[Suitekey][testCaseKey].Actions;

                        it(TCID + ' : ' + TC_NAME + ' -- Tags: ' + TC_Tags, function(client) {
                            console.log("\n***** Starting " + TCID + '-' + TC_NAME + ' -- Tags: ' + TC_Tags + " *****");
                            const actionIterator = actionsToPerform.length;
                            console.log("Total Number of Actions to be performed:" + actionIterator);
                            const AA = actionsToPerform;
                            testcaseExecution(TC_RequirePreCheck, actionIterator, AA, TC_SkipValidationProperty, client);
                        })
                    }
                })

                after(function(client, done) {
                    client.end(function() {
                        console.log("******************** End of Test Suite ********************\n\n")
                        done();
                    });
                });

            });

        })
    })
});