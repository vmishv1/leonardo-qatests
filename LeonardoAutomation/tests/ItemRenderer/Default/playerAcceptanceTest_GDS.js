var fs = require('fs');
var getSelector = require(currentDirPath + '/utils/modules/getSelectorForCell');
global.configFile = JSON.parse(fs.readFileSync(currentDirPath + '/testdata/playerTestData.json'));
GDS = configFile["GDS"];
describe ('QI_TS03 -GDS Validation', function(client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
    var testcase_name;

	before(function(client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)		
		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)
		
		embedPlayer = client.page.embedPlayer();
		
		client.launchURL("GDS", client, done)
		client.pause(20000)
	});

	beforeEach(function (client, done) {
        testcase_name = this.currentTest.title;
        logger.info("Executing following Test Case: " + testcase_name)
        done();
    });

	it('TC01 - Check cell is editable and cell text', function(client) {
		
		embedPlayer
			.selectCell('A1')
			.verify.containsText("div.leo-canvasarea .k-spreadsheet-cell.k-spreadsheet-active-cell", GDS.cellData)
	})

	it('TC02 - Check all error scenario', function(client) {

		embedPlayer
			.click(properties.get('checkMyWorkButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 14)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
	});

	it('TC03 - Validate Feedback Header Text and Tooltip for cell B6', function(client) {

		embedPlayer
			.validateTooltipForCell('A2', GDS.feedbackText)
			.validateFeedbackHeaderTextForCell('A2', GDS.feedbackHeader)
	});

	it('TC04 - Validate Try Again functionality', function(client) {

		embedPlayer
			.click(properties.get('tryAgainButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
	});

	it('TC05 - Check My Work for few correct and few incorrect cell values - Text, Accounting, Percent and Decimal Values', function(client) {

		embedPlayer.api
			.selectCell('B3')
			.keys(client.Keys.DELETE)
			.enterText(GDS.text1)
			.selectCell('D3')
			.keys(client.Keys.DELETE)
			.enterText(GDS.text2)
			.click(properties.get('checkMyWorkButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 2)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 12)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0);
	});

	it('TC06 - Validate Try Again does not reset the cell values - Check for cell B1', function(client) {

		embedPlayer	
			.validateTooltipForCell('H2', GDS.toolTipText)
			.click(properties.get('tryAgainButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
			.verify.containsText(getSelector.getSelectorForCell('B3'), GDS.text1)
	});


	it('TC07 - Validate Reset functionality removes all the user entered values', function(client) {

		embedPlayer	
			.click(properties.get('tryAgainButton'))
			.click(properties.get('resetButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
			.selectCell('B3')
			.verify.containsText(getSelector.getSelectorForCell('B3'), "")

	});

	afterEach(function (client, done) {
        testcase_name = this.currentTest.title;
        logger.info("Completed following Test Case: " + testcase_name)
        client.pause(2000, function() {
	        done();
        	
        })
    });

	after(function(client, done) {
		logger.info("Completed following suite : " + suite_name)
    	client.end(function() {
      		done();
    	});
  	});
});