var fs = require('fs');
var getSelector = require(currentDirPath + '/utils/modules/getSelectorForCell');
configFile = JSON.parse(fs.readFileSync(currentDirPath + '/testdata/playerTestData.json'));
VerticalView = configFile["playerAcceptanceTest_HBP"];
describe('QI_TS03 - Vertical View Template - Single Sheet With Row, Col header, Gridlines', function (client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
	var testcase_name;

	before(function (client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)
		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)

		embedPlayer = client.page.embedPlayer();
		previewPublishPage = client.page.previewPublishPage();

		client.launchURL("playerAcceptanceTest_VerticalView", client, done)
		client.pause(5000)
	});

	beforeEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Executing following Test Case: " + testcase_name)
		done();
	});

	it('TC01 - Check cell is editable and cell text', function (client) {

		embedPlayer
		.selectCell('A26')
			.selectCell('A2')
			.verify.containsText("div.leo-canvasarea .k-spreadsheet-cell.k-spreadsheet-active-cell", VerticalView.cellData)
			.verify.cssClassNotPresent('div.leo-canvasarea .k-spreadsheet-cell.k-spreadsheet-active-cell', 'k-state-disabled')
	})

	it('TC02 - Check all error scenario', function (client) {

		embedPlayer
			.selectCell('A26')
			.click(properties.get('checkMyWorkButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 34)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
	});

	it('TC03 - Validate Feedback Header Text and Tooltip for cell B6', function (client) {

		embedPlayer
			.validateTooltipForCell('B6', VerticalView.feedbackText)
			.validateFeedbackHeaderTextForCell('B6', VerticalView.feedbackHeader)
	});

	it('TC04 - Validate Try Again functionality', function (client) {

		embedPlayer
			.click(properties.get('tryAgainButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
	});

	it('TC05 - Check My Work for few correct and few incorrect cell values - Text, Accounting, Percent and Decimal Values', function (client) {

		embedPlayer.api
			.selectCell('B1')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.text1)
			.selectCell('B6')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.text2)
			.selectCell('B7')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.text3)
			.selectCell('B12')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.text4)
			.click(properties.get('checkMyWorkButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 16)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 18)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0);
	});

	it('TC06 - Validate Try Again does not reset the cell values - Check for cell B1', function (client) {

		embedPlayer
			.validateTooltipForCell('E22', VerticalView.toolTipText)
			.click(properties.get('tryAgainButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
			.verify.containsText(getSelector.getSelectorForCell('B1'), VerticalView.text1)
	});

	it('TC07 - Check My Work after entering Formulas in cells - Perfect Scenario', function (client) {

		client
			.selectCell('D18')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.formula1)
			.selectCell('F18')
			.keys(client.Keys.DELETE)
			.enterText(VerticalView.formula2)
			.click(properties.get('checkMyWorkButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 34)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0);
	});

	it('TC08 - Validate Reset functionality removes all the user entered values', function (client) {

		embedPlayer
			.click(properties.get('tryAgainButton'))
			.click(properties.get('resetButton'))
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			.verify.elementCount(embedPlayer.elements.partialCorrectCells.selector, 0)
			.verify.containsText(getSelector.getSelectorForCell('B1'), "")

	});

	it('TC09 - Validate Instructions Area', function (client) {

		previewPublishPage
			.verify.elementPresent('.instruction-spreadsheet-component', 'Embed Item is not present')
			.validateTextForRow(VerticalView.VerticalViewInsText, 1)
			.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is Editable in Instructions Area')
			.verify.containsText('div.k-spreadsheet-cell.k-state-disabled.k-row-0.k-col-0', VerticalView.A1cellData)

	})

	afterEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Completed following Test Case: " + testcase_name)
		client.pause(2000, function () {
			done();

		})
	});

	after(function (client, done) {
		logger.info("Completed following suite : " + suite_name)
		client.end(function () {
			done();
		});
	});
});