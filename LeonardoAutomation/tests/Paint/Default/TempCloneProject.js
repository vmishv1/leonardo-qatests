var getItemID = require(currentDirPath + '/utils/modules/getID');
var getEmbedID = require(currentDirPath + '/utils/modules/embedUrl');


describe('Clone the Multiple Sheet Question Item Acceptance Test', function (client) {

  this.timeout(9000000);
  var testfile_name = this.file;
  var suite_name = this.title;
  var testcase_name;
  var n = testfile_name.lastIndexOf('\\');
  var result = testfile_name.substring(n + 1);
  var ItemId, client1, embedurl;
  var item = "presentation"

  before(function (client, done) {

    logger.info("Executing File: " + result + ".js")
    logger.info("Starting following suite : " + suite_name)
    loginPage = client.page.loginPage();
    createItem = client.page.createItem();
    setupPage = client.page.setupPage();
    templatePage = client.page.templatePage();
    instructionsPage = client.page.instructionsPage();
    preferencePage = client.page.preferencePage();
    documentUploadPage = client.page.documentUpload();
    scoringRulesPage = client.page.scoringRulesPage();
    previewPublishPage = client.page.previewPublishPage();
    dashBoardPage = client.page.dashBoardPage();
    embedPlayer = client.page.embedPlayer();

    logger.info("Launching Browser : " + client.options.desiredCapabilities.browserName)
    
    client.launchURL(client, done);
  })

  beforeEach(function (client, done) {
    testcase_name = this.currentTest.title;
    testcase_name = testcase_name.substring(testcase_name.lastIndexOf("\\") + 1)
    logger.info("Executing following Test Case: " + testcase_name.substring(testcase_name.lastIndexOf('\\') + 1))
    done();
  });

  it('TC01: Validate "Clone Item" workflow for Multiple Sheet Question Item', function (client) {

    client
      .waitUntilLoaderPresent(function () {
        dashBoardPage
          .searchProject("Question_Item_With_Multiple_Sheets")
          .sortList("Recently Used")
          .cloneFirstItemofSearchList()
      })
      .waitUntilLoaderPresent(function () {
        setupPage
          .verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
          .verify.containsText('#other div.tag-sel-item', "Other Tag", "Tag not entered correctly")
          .updateTitle("Clone Check")
          .click(properties.get("nextButton"))
      })
      .waitUntilLoaderPresent(function () {
        templatePage
          .verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active")
          .changePreference("Splitter Visibility")
          .click(properties.get("nextButton"))

      })
      .waitUntilLoaderPresent(function () {
        logger.info("Validate the Instruction Page")
        instructionsPage
          .verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active")
          .verify.containsText('.instructions-editor  p:nth-child(1)', "Instruction 1 Text", "Intruction Data is not same")

      })
      .pause(10000)
      .frame(0)
      .waitForElementVisible('.leohost .k-spreadsheet-active-cell div', 40000)
      .verify.containsText('.leohost .k-spreadsheet-active-cell div', 'Cruise', 'Text inside the Embedded Item is not visible')
      .verify.cssClassPresent('.leohost .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
      .frameParent()
      .verify.elementPresent('div.embedded-preview-container', "Embedded item not displayed correctly")
      .click(properties.get("nextButton"))
      .waitUntilLoaderPresent(function () {
        documentUploadPage
          .verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active")
          .downloadDocument(2)
          .uploadDocument("MultiSheet_Start.xlsx", "MultiSheet_Finalupdated.xlsx", "Map")
          .verify.containsText(documentUploadPage.elements.submissionStatus1.selector, String("Uploaded on").trim())
          .click(properties.get("nextButton"))


      })
      .waitUntilLoaderPresent(function () {
        scoringRulesPage
          .verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active")
          .applyWarningMessage()
          .click(properties.get("previewButton"))
        previewPublishPage
          .validateTextForRow("Instruction 1 Text", 1)
          .closePreview()
          .click(properties.get("nextButton"))

      })
      .waitUntilLoaderPresent(function () {
        previewPublishPage
          .verify.containsText(properties.get("activeTabText"), "Preview & Publish", "Preview & Publish Tab is not active")
          .validatePublishCheckpoints()
          .openPreview()
          .validateTextForRow("Instruction 3 Text", 3)
          .verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item');
        embedPlayer
          .verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
          .verify.containsText('.leo-canvasarea .k-spreadsheet-active-cell div', '400', 'Text is not correct inside the Player window')
          .click(properties.get('checkMyWorkButton'))

        previewPublishPage
          .closePreview()
          .publishItem()
          .api.useXpath()
          .verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
          .verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
          .useCss();

        getItemID
          .getLeonardoID(client)
          .then(function (value) {
            logger.info("Item ID: " + value);
            ItemId = value
          })
      })
      .click(properties.get("finishButton"))
  })

  afterEach(function (client, done) {
    testcase_name = this.currentTest.title;
    logger.info("Completed following Test Case: " + testcase_name)
    done();
  });

  after(function (client, done) {
    client.end(function () {
      done();
    })
  })
})