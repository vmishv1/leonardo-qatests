var fs = require('fs');

var getItemID = require(currentDirPath + '/utils/modules/getID');
var getEmbedID = require(currentDirPath + '/utils/modules/embedUrl');
configFile = JSON.parse(fs.readFileSync(currentDirPath + '/testdata/CellFormattingData.json'));
testData = configFile["Cells"];
const chai = require('chai');
describe('Scoring page Cell Formatting Data Test', function (client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
	var testcase_name;
	var ItemId;
	var embedID;
	var test;
	global.testfile_name1 = this.file;
	before(function (client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)

		loginPage = client.page.loginPage();
		createItem = client.page.createItem();
		setupPage = client.page.setupPage();
		templatePage = client.page.templatePage();
		instructionsPage = client.page.instructionsPage();
		preferencePage = client.page.preferencePage();
		documentUploadPage = client.page.documentUpload();
		scoringRulesPage = client.page.scoringRulesPage();
		previewPublishPage = client.page.previewPublishPage();
		headerFooterPage = client.page.headerFooterPage();
		dashBoardPage = client.page.dashBoardPage();
		embedPlayer = client.page.embedPlayer();

		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)

		client.launchURL(client, done);
		var title = "Check Scoring page data"

		client
			.url(envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url)
			.waitUntilLoaderPresent(function () {
				createItem
					.pause(2000)
					.openCreate()
					.verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
					.verify.cssClassPresent(createItem.elements.presetationItem.selector, 'modeItem', 'Presentation Item is Not Active')
					.createItem(title, "question");
			})
			.waitUntilLoaderPresent(function () {
				setupPage
					.verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
					.click(properties.get("nextButton"))
			})
			.waitUntilLoaderPresent(function () {
				templatePage
					.verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active")
					.chooseTemplate(2)
					.click(properties.get("nextButton"))
			})
			.waitUntilLoaderPresent(function () {
				instructionsPage
					.verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active")
					.click(properties.get("nextButton"))
			})
			.waitUntilLoaderPresent(function () {
				documentUploadPage
					.verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active")
					.pause(2000)
					.uploadDocument("Testdata_gdsTranslator_start.xlsx", "Testdata_gdsTranslator final.xlsx")
					.pause(2000)
					.click(properties.get("nextButton"))
			})
			.waitUntilLoaderPresent(function () {
				scoringRulesPage
					.verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active")
			})
	});

	beforeEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Executing following Test Case: " + testcase_name)
		done();
	});

	it('TC01 - Validate Bold Property for true case', function (client) {
		client.getFormattingData('1', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[0].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[0].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[0].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[0].properties[0].propValue)
		});
	});

	it('TC04 - italics property value for false case', function (client) {
		client.getFormattingData('2', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[1].sheetName)
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[1].cellName)
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[1].properties[0].propName)
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[1].properties[0].propValue)
		});
	});

	it('TC05 - Validate underline property value for false case', function (client) {
		client.getFormattingData('3', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[2].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[2].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[2].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[2].properties[0].propValue)
		});
	});

	it('TC07 - Validate underline property value for double case', function (client) {
		client.getFormattingData('4', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[3].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[3].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[3].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[3].properties[0].propValue)
		});
	});

	it('TC08 - Validate vertical alignment property value for top case', function (client) {
		client.getFormattingData('5', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[4].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[4].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[4].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[4].properties[0].propValue)
		});
	});

	it('TC16 - Validate horizontal alignment property value for center case', function (client) {
		client.getFormattingData('6', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[5].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[5].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[5].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[5].properties[0].propValue)
		});
	});

	it('TC21 - Validate wrap property value for true case', function (client) {
		client.getFormattingData('7', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[6].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[6].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[6].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[6].properties[0].propValue)
		});
	});
	it('TC22 - Validate wrap property value for false case', function (client) {
		client.getFormattingData('8', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[7].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[7].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[7].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[7].properties[0].propValue)
		});
	});
	it('TC23 - Validate color property value for Standard color 1', function (client) {
		client.getFormattingData('9', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[8].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[8].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[8].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[8].properties[0].propValue)
		});
	});
	it('TC35 - Validate color property value for any custom color', function (client) {
		client.getFormattingData('10', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[9].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[9].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[9].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[9].properties[0].propValue)
		});
	});
	it('TC37 - Validate background_color property value for Standard color 2', function (client) {
		client.getFormattingData('11', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[10].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[10].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[10].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[10].properties[0].propValue);
			scoringRulesPage.verify.equal(output.Cells[0].properties[1].propName, testData[10].properties[1].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[1].propValue, testData[10].properties[1].propValue)
		});
	});
	it('TC47 - Validate background_color property value for any theme color', function (client) {
		client.getFormattingData('12', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[11].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[11].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[11].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[11].properties[0].propValue);
			scoringRulesPage.verify.equal(output.Cells[0].properties[1].propName, testData[11].properties[1].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[1].propValue, testData[11].properties[1].propValue)
		});
	});
	it('TC49 - Validate format property value for text', function (client) {
		client.getFormattingData('13', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[12].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[12].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[12].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[12].properties[0].propValue)
		});
	});
	it('TC70 - Validate format property value for Time(hh:mm:ss)', function (client) {
		client.getFormattingData('14', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[13].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[13].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[13].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[13].properties[0].propValue)
		});
	});
	it('TC72 - Validate format property value for Time(hh:mm)', function (client) {
		client.getFormattingData('15', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[14].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[14].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[14].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[14].properties[0].propValue)
		});
	});
	it('TC77 - Validate format property value for Percentage', function (client) {
		client.getFormattingData('16', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[15].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[15].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[15].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[15].properties[0].propValue)
		});
	});
	it('TC80 - Validate format property value for Custom number', function (client) {
		client.getFormattingData('17', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[16].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[16].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[16].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[16].properties[0].propValue)
		});
	});
	it('TC86 - Validate format property value for General', function (client) {
		client.getFormattingData('18', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[17].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[17].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[17].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[17].properties[0].propValue)
		});
	});
	it('TC88 - Validate property value for positive Number formatting', function (client) {
		client.getFormattingData('19', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[18].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[18].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[18].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[18].properties[0].propValue)
		});
	});

	it('TC129 - Validate property value for Fraction formatting', function (client) {
		client.getFormattingData('20', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[19].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[19].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[19].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[19].properties[0].propValue)
		});
	});

	it('TC138 - Validate property value for Percentage formatting', function (client) {
		client.getFormattingData('21', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[20].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[20].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[20].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[20].properties[0].propValue)
		});
	});

	it('TC152 - Validate property value for Text formatting', function (client) {
		client.getFormattingData('22', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[21].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[21].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[21].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[21].properties[0].propValue)
		});
	});

	it('TC97 - Validate property value for Accounting formatting 1', function (client) {
		client.getFormattingData('23', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[22].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[22].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[22].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[22].properties[0].propValue)
		});
	});

	it('TC112 - Validate property value for Date formatting(Long date)', function (client) {
		client.getFormattingData('24', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[23].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[23].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[23].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[23].properties[0].propValue)
		});
	});
	it('TC142 - Validate property value for Scientific formatting', function (client) {
		client.getFormattingData('25', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[24].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[24].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[24].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[24].properties[0].propValue)
		});
	});
	it('TC156 - Validate property value for general formatting', function (client) {
		client.getFormattingData('26', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[25].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[25].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[25].properties[0].propName);
			chai.assert.equal(output.Cells[0].properties[0].propValue, testData[25].properties[0].propValue)
		});
	});
	it('TC105 - Validate property value for Currency formatting 1', function (client) {
		client.getFormattingData('27', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[26].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[26].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[26].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[26].properties[0].propValue)
		});
	});
	it('TC100 - Validate property value for Accounting formatting 2', function (client) {
		client.getFormattingData('28', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[27].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[27].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[27].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[27].properties[0].propValue)
		});
	});
	it('TC107 - Validate property value for Currency formatting 2', function (client) {
		client.getFormattingData('29', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[28].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[28].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[28].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[28].properties[0].propValue)
		});
	});
	it('TC151 - Validate property value for Time formatting', function (client) {
		client.getFormattingData('30', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[29].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[29].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[29].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[29].properties[0].propValue)
		});
	});
	it('TC111 - Validate property value for Date formatting(Short date)', function (client) {
		client.getFormattingData('31', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[30].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[30].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[30].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[30].properties[0].propValue)
		});
	});
	it('TC94 - Validate property value for negative Number formatting', function (client) {
		client.getFormattingData('32', function (output) {
			console.log("output : " + JSON.stringify(output.Cells));
			scoringRulesPage.verify.equal(output.Cells[0].sheetName, testData[31].sheetName);
			scoringRulesPage.verify.equal(output.Cells[0].cellName, testData[31].cellName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propName, testData[31].properties[0].propName);
			scoringRulesPage.verify.equal(output.Cells[0].properties[0].propValue, testData[31].properties[0].propValue)
		});
	});

	after(function (client, done) {
		logger.info("Completed following suite : " + suite_name)
		client.end(function () {
			done();
		});
	});

})