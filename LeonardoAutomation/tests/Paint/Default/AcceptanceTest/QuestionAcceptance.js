var getItemID = require(currentDirPath + '/utils/modules/getID');
var getEmbedID = require(currentDirPath + '/utils/modules/embedUrl');
describe('Question Item Acceptance Test', function (client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
	var testcase_name;
	var ItemId;
	var embedID;
	global.testfile_name1 = this.file;
	before(function (client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)

		loginPage = client.page.loginPage();
		createItem = client.page.createItem();
		setupPage = client.page.setupPage();
		templatePage = client.page.templatePage();
		instructionsPage = client.page.instructionsPage();
		preferencePage = client.page.preferencePage();
		documentUploadPage = client.page.documentUpload();
		scoringRulesPage = client.page.scoringRulesPage();
		previewPublishPage = client.page.previewPublishPage();
		headerFooterPage = client.page.headerFooterPage();
		dashBoardPage = client.page.dashBoardPage();
		embedPlayer = client.page.embedPlayer();

		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)

		client.launchURL(client, done);
	});

	beforeEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Executing following Test Case: " + testcase_name)
		done();
	});

	it('TC01 - Question Workflow Validation using workbook having multiple sheets', function (client, done) {
		var title = "Question_Item_With_Multiple_Sheets"
		client
			.waitUntilLoaderPresent(function () {
				createItem
					.openCreate()
					.verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
					.verify.cssClassPresent(createItem.elements.presetationItem.selector, 'modeItem', 'Presentation Item is Not Active')
					.createItem(title, "question");
			})
			.waitUntilLoaderPresent(function () {
				setupPage
					.verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
					.verify.value('#title', title, 'SetUp Page Title is not Correct')
					.enterSetupDetails("This is the description", "Other Tags")
					.verify.containsText('#tag-sel-ctn-2', "Other Tags", "Tag not entered correctly")
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				templatePage
					.verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active")
					.verify.cssClassPresent(templatePage.getSelectorForTemplate(1), "template-selected", "Default Template is not selected")
					.verify.cssClassPresent(templatePage.getSelectorForLayout(1), "layout-selected", "Default Layout is not selected")
					.chooseTemplate(1)
					.chooseLayout(2)
					.click(properties.get("nextButton"));

			})

			.waitUntilLoaderPresent(function () {
				instructionsPage
					.verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active")
					.clickRow(1)
					.enterInstructionText("Instruction 1 Text")
					.clickRow(2)
					.enterInstructionText("Instruction 2 Text")
					.clickRow(3)
					.enterInstructionText("Instruction 3 Text")
					.selectTextForRow(2)
					.selectFormattingToolbarOption(1)
					.verify.elementPresent('div.content-area p:nth-child(2) b')
					.selectFormattingToolbarOption(3)
					.verify.elementPresent('div.content-area p:nth-child(2) u')
					.clickRow(4)
					.addSubItem()
					.selectSubItemOption(1)
			})

			.waitUntilLoaderPresent(function () {
				instructionsPage
					.api.frame(0)
					.waitForElementVisible(properties.get("nextButton"))
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				documentUploadPage
					.uploadDocument('Motion_Profile.xlsx')
					.pause(10000)
					//.verify.containsText(documentUpload.elements.submissionStatus1.selector, String("Uploaded on").trim())
          
					.waitForElementVisible(properties.get("nextButton"))
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				preferencePage
					.waitForElementPresent('div.leo-presentationwidget', 20000)
					.selectSheet("Building Profile")
					.cellRange("A5", "E10")
					.enableToggleButton("Formula Bar")
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				previewPublishPage
					.validatePublishCheckpoints()
					.verify.cssClassPresent('button.publish-btn', 'disabled', 'Publish button is not disabled in the Embed Item')
			})

			.click(properties.get("finishButton"))
			.frameParent()
			.frame(0)
			.waitForElementVisible('.leohost .k-spreadsheet-active-cell div', 40000)
			.verify.containsText('.leohost .k-spreadsheet-active-cell div', 'Cruise', 'Text inside the Embedded Item is not visible')
			.verify.cssClassPresent('.leohost .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
			.frameParent()
			.verify.elementPresent('div.embedded-preview-container', "Embedded item not displayed correctly")
			.click(properties.get("nextButton"))
			.waitUntilLoaderPresent(function () {
				documentUploadPage
					.verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active")
					.uploadDocument("MultiSheet_Start.xlsx", "MultiSheet_Final.xlsx")
					.verify.containsText(documentUploadPage.elements.submissionStatus1.selector, String("Uploaded on").trim())
					.openPreviewDocument('1')
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Initial Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '400')
					.verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
					.closePreviewDocument()
					.openPreviewDocument('2')
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Final Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '500')
					.verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
					.closePreviewDocument()
					.click(properties.get("nextButton"));
			})

			.waitUntilLoaderPresent(function () {
				scoringRulesPage
					.verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active")
					.toggleScore(2)
					.verify.value("div.score-heading  input", "92", "Score is not matching")
					.addFeedbackText("Feedback updated", 5)
					.verify.elementCount('i.active-action', 1, "Feedback text not added successfully")
					.addHint("This is the hint text", 2, 8)
					.verify.elementCount('i.active-action', 2, "Hint not added successfully")
					.addNewRule("A4", 5)
					.verify.elementCount("div.table-row", 13, "New Rule not added successfully")
				scoringRulesPage
					.waitForElementVisible(scoringRulesPage.elements.hintSummary.selector, 20000)
					.click(scoringRulesPage.elements.hintSummary.selector)
					.verify.containsText('span.total-hints.ml-2', '1')
					.verify.containsText('span#cells', 'Cell A2')
					.waitForElementVisible('i.fa.fa-pencil', 10000)
					.click('i.fa.fa-pencil')
					.waitForElementVisible('.hint-body #add-hint-wrapper', 10000)
					.click('.hint-body #add-hint-wrapper')
					.addHintfromhintsummary("This is the Add new hint text", "2")
					.waitForElementVisible(documentUploadPage.elements.previewClose.selector, 20000)
					.click(documentUploadPage.elements.previewClose.selector)
					.api.pause(2000)
				scoringRulesPage
					.waitForElementVisible(scoringRulesPage.elements.settings.selector, 20000)
					.click(scoringRulesPage.elements.settings.selector)
					.hintTypeSelection('Question-level')
					.waitForElementVisible(scoringRulesPage.elements.hintSummary.selector, 20000)
					.click(scoringRulesPage.elements.hintSummary.selector)
					.verify.containsText('span.total-hints.ml-2', '2')
					.verify.containsText('.d-flex.header-one .label', 'Hint 1')
					.deleteHintfromhintsummary()
					.verify.containsText('span.total-hints.ml-2', '1')
					.waitForElementVisible(documentUploadPage.elements.previewClose.selector, 20000)
					.click(documentUploadPage.elements.previewClose.selector)
					.api.pause(2000)
				scoringRulesPage
					.waitForElementVisible(scoringRulesPage.elements.settings.selector, 20000)
					.click(scoringRulesPage.elements.settings.selector)
					.hintTypeSelection('Cell-level')
					.api.pause(2000)
				scoringRulesPage
					.waitForElementVisible(scoringRulesPage.elements.initialDocument.selector, 10000)
					.click(scoringRulesPage.elements.initialDocument.selector)
					.waitForElementVisible(documentUploadPage.elements.previewWindowHeader.selector, 10000)
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Initial Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '400')
					.verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')


				documentUploadPage
					.closePreviewDocument()
					.click(scoringRulesPage.elements.finalDocument.selector)
					.waitForElementVisible(documentUploadPage.elements.previewWindowHeader.selector, 10000)
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Final Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '500')
					.verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Document cell is not disabled')
					.closePreviewDocument()
					.click(properties.get("previewButton"));
				previewPublishPage
					.validateTextForRow("Instruction 1 Text", 1)
					.closePreview()
					.click(properties.get("nextButton"));
			})

			.waitUntilLoaderPresent(function () {
				previewPublishPage
					.verify.containsText(properties.get("activeTabText"), "Preview & Publish", "Preview & Publish Tab is not active")
					.validatePublishCheckpoints()
					.pause(10000)
					.openPreview()
					.validateTextForRow("Instruction 3 Text", 3)
					.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item');
				embedPlayer
					.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
					.verify.containsText('.leo-canvasarea .k-spreadsheet-active-cell div', '400', 'Text is not correct inside the Player window')
					
				.waitForElementVisible(properties.get('showHint'), 20000)
					.click(properties.get('showHint'))
					.verify.elementCount(embedPlayer.elements.activeHintCells.selector, 1)
					.selectCell('A2')
					.waitForElementVisible(embedPlayer.elements.remainingHintsCount.selector, 10000)
					.verify.containsText(embedPlayer.elements.remainingHintsCount.selector, '1')
					.waitForElementVisible(embedPlayer.elements.revealHint.selector,10000)
					.click(embedPlayer.elements.revealHint.selector)
					client.verify.not.elementPresent(embedPlayer.elements.remainingHintsCount.selector)
				embedPlayer
					.validateHintTextForCell('A2', 1, "Hint 1:This is the hint text")
					.validatePenaltyForCell('A2', 1, 2)
					.waitForElementPresent(embedPlayer.elements.closeHintContainer.selector)
					.click(embedPlayer.elements.closeHintContainer.selector)
					.click(properties.get('checkMyWorkButton'))
					.selectCell('A1')
					.validateTooltipForCell('A1', 'A1: Feedback updated', 1)
					.validateTooltipForCell('A1', 'A1: Bold should be applied.', 2);
				previewPublishPage
					.closePreview()
					.publishItem()
					.api.useXpath()
					.pause(20000)
			
						
					.verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
				//	.verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
					.useCss();

				getItemID
					.getLeonardoID(client)
					.then(function (value) {
						logger.info("Item ID: " + value);
						ItemId = value
					})
			})
			.waitForElementVisible(properties.get("finishButton"))
			.click(properties.get("finishButton"))
			.waitUntilLoaderPresent(function () {
				dashBoardPage
					.openItemPreview(ItemId)
					.waitForElementPresent(dashBoardPage.elements.previewQuestionActiveCellData.selector, 10000)
					.verify.containsText(dashBoardPage.elements.previewQuestionActiveCellData.selector, '400', 'Text is not visible')
					.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
				previewPublishPage
					.closePreview()
			})

			/*.perform(function () {
				dashBoardPage
					.openEmbed(ItemId)
				getEmbedID
					.getEmbedUrl(client)
					.then(function (value) {
						logger.info("Embed Item ID: " + value);
						client.url(value, function () {
							client.waitForElementVisible('div.leo-widget', 20000)
						})
					})

			})

			.perform(function () {
				embedPlayer
					.waitForElementVisible('div.leo-widget', 20000)
					//.verify.cssClassPresent('.leo-instructionarea .k-spreadsheet-active-cell', 'k-state-disabled', 'Cell is editable inside the Embedded item')
					.verify.containsText('.leo-canvasarea .k-spreadsheet-active-cell div', '400', 'Text is not correct inside the Player window')
					.waitForElementVisible(properties.get('showHint'), 10000)
					.click(properties.get('showHint'))
					.verify.elementCount(embedPlayer.elements.activeHintCells.selector, 1)
					.selectCell('A2')
					.waitForElementVisible(embedPlayer.elements.remainingHintsCount.selector, 10000)
					.verify.containsText(embedPlayer.elements.remainingHintsCount.selector, '1')
					.click(embedPlayer.elements.revealHint.selector)
					client.verify.not.elementPresent(embedPlayer.elements.remainingHintsCount.selector)
				embedPlayer
					.validateHintTextForCell('A2', 1, "")
					.validatePenaltyForCell('A2', 1, 2)
					.click(embedPlayer.elements.closeHintContainer.selector)
					.click(properties.get('checkMyWorkButton'))
					.selectCell('A1')
					.validateTooltipForCell('A1', 'A1: Feedback updated', 1)
					.validateTooltipForCell('A1', 'A1: Bold should be applied.', 2)
			})*/
	});

	it('TC02 - Question Workflow Validation using workbook having single sheet', function (client) {
		var title = "Question-Item - With Single Sheet"
		client
			.url(envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url)
			.waitUntilLoaderPresent(function () {
				createItem
					.openCreate()
					.verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
					.verify.cssClassPresent(createItem.elements.presetationItem.selector, 'modeItem', 'Presentation Item is Not Active')
					.createItem(title, "question");
			})
			.waitUntilLoaderPresent(function () {
				setupPage
					.verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
					.verify.value('#title', title, 'SetUp Page Title is not Correct')
					.enterSetupDetails("This is the description", "Other Tags")
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				templatePage
					.verify.containsText(properties.get("activeTabText"), "Template", "Template Tab is not active")
					.verify.cssClassPresent(templatePage.getSelectorForTemplate(1), "template-selected", "Default Template is not selected")
					.verify.cssClassPresent(templatePage.getSelectorForLayout(1), "layout-selected", "Default Layout is not selected")
					.chooseTemplate(2)
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				instructionsPage
					.verify.containsText(properties.get("activeTabText"), "Instructions", "Instructions Tab is not active")
					.clickRow(1)
					.enterInstructionText("Instruction 1 Text")
					.clickRow(2)
					.enterInstructionText("Instruction 2 Text")
					.clickRow(3)
					.enterInstructionText("Instruction 3 Text")
					.selectTextForRow(2)
					.selectFormattingToolbarOption(1)
					.verify.elementPresent('div.content-area p:nth-child(2) b')
					.selectFormattingToolbarOption(3)
					.verify.elementPresent('div.content-area p:nth-child(2) u')
					.clickRow(3)
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				documentUploadPage
					.verify.containsText(properties.get("activeTabText"), "Documents", "Documents Tab is not active")
					.uploadDocument("testfile.xlsx", "testfile_Final.xlsx")
					.verify.containsText(documentUploadPage.elements.submissionStatus1.selector, String("Uploaded on").trim())
					.openPreviewDocument('1')
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Initial Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '1')
					.closePreviewDocument()
					.openPreviewDocument('2')
					.verify.containsText(documentUploadPage.elements.previewWindowHeader.selector, 'Final Document')
					.verify.containsText(documentUploadPage.elements.activeCellData.selector, '1')
					.closePreviewDocument()
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				scoringRulesPage
					.verify.containsText(properties.get("activeTabText"), "Scoring Rules", "Scoring Rules Tab is not active")
					.toggleScore(1)
					.verify.value("div.score-heading  input", "80", "Score is not matching")
					.click(properties.get("previewButton"));
				previewPublishPage
					.validateTextForRow("Instruction 1 Text", 1)
					.closePreview()
					.click(properties.get("nextButton"))
			})

			.waitUntilLoaderPresent(function () {
				previewPublishPage
					.verify.containsText(properties.get("activeTabText"), "Preview & Publish", "Preview & Publish Tab is not active")
					.validatePublishCheckpoints()
					.openPreview()
					.validateTextForRow("Instruction 3 Text", 3)
					.closePreview()
					.publishItem()
					.api.useXpath()
					.pause(20000)
					.verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
					//.verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
					.useCss();

				getItemID
					.getLeonardoID(client)
					.then(function (value) {
						logger.info("Item ID:" + value);
						ItemId = value
					})
			})
			.waitForElementVisible(properties.get("finishButton"))
			.click(properties.get("finishButton"))
			.waitUntilLoaderPresent(function () {
				dashBoardPage
					.openItemPreview(ItemId)
					.waitForElementPresent(dashBoardPage.elements.previewQuestionActiveCellData.selector, 10000)
					.verify.containsText(dashBoardPage.elements.previewQuestionActiveCellData.selector, '1', 'Text is not visible');
				previewPublishPage
					.closePreview()
			})
	});
			
	afterEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Completed following Test Case: " + testcase_name)
		done();
	});

	after(function (client, done) {
		logger.info("Completed following suite : " + suite_name)
		client.end(function () {
			done();
		});
	});

})