var getItemID = require(currentDirPath + '/utils/modules/getID');
var getEmbedID = require(currentDirPath + '/utils/modules/embedUrl');


describe('Presentation Item Acceptance Test', function (client) {

  this.timeout(9000000);
  var testfile_name = this.file;
  var suite_name = this.title;
  var testcase_name;
  var n = testfile_name.lastIndexOf('\\');
  var result = testfile_name.substring(n + 1);
  var ItemId, client1, embedurl;
  var item = "presentation"

  before(function (client, done) {

    logger.info("Executing File: " + result + ".js")
    logger.info("Starting following suite : " + suite_name)
    loginPage = client.page.loginPage();
    createItem = client.page.createItem();
    setupPage = client.page.setupPage();
    documentUpload = client.page.documentUpload();
    preferencePage = client.page.preferencePage();
    previewPublishPage = client.page.previewPublishPage();
    headerFooterPage = client.page.headerFooterPage();
    dashBoardPage = client.page.dashBoardPage();

    logger.info("Launching Browser : " + client.options.desiredCapabilities.browserName)
    
    client.launchURL(client, done);
  })

  beforeEach(function (client, done) {
    testcase_name = this.currentTest.title;
    testcase_name = testcase_name.substring(testcase_name.lastIndexOf("\\") + 1)
    logger.info("Executing following Test Case: " + testcase_name.substring(testcase_name.lastIndexOf('\\') + 1))
    done();
  });

  it('TC01: Validate "Create Presentation Item" workflow for a single sheet workbook', function (client) {
    logger.info("Launching the Leonardo Paint")
    var Title = "leonardo-Single Sheet-Presentation-Test"

    client
      .waitUntilLoaderPresent(function () {
        createItem
          .openCreate()
          .verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
          .verify.cssClassPresent(createItem.elements.presetationItem.selector, 'modeItem', 'Presentation Item is Active')
          .createItem(Title, item)
      })

      .waitUntilLoaderPresent(function () {
        setupPage
          .verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
          .verify.value('#title', Title, 'SetUp Page Title is not Correct')
          .enterSetupDetails("Description", "Other Tag")
          .verify.containsText('#tag-sel-ctn-2', "Other Tag", "Tag not entered correctly")
          .click(properties.get("nextButton"))
      })

      .waitUntilLoaderPresent(function () {
        documentUpload
          .verify.containsText(properties.get("activeTabText"), "Document", "Documents Tab is not active")
          .uploadDocument('Motion_Profile.xlsx')
          .verify.containsText(documentUpload.elements.submissionStatus1.selector, String("Uploaded on").trim())
          .pause(10000)
          .openPreviewDocument('1')
          .verify.containsText(documentUpload.elements.previewWindowHeader.selector, 'Presentation Document')
          .verify.containsText(documentUpload.elements.activeCellData.selector, 'Segment Type')
        dashBoardPage
          .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
        documentUpload
          .closePreviewDocument()
          .click(properties.get("nextButton"))
      })

      .waitUntilLoaderPresent(function () {

        preferencePage
          .verify.containsText(properties.get("activeTabText"), "Preference", "Preference Tab is not active")
          .waitForElementPresent('div.leo-presentationwidget', 20000)
          .selectSheet("Building Profile")
          .verify.containsText(preferencePage.elements.activeSheet.selector, "Building Profile", "Sheet is not selected correctly")
          .cellRange("A5", "E16")
          .verify.cssClassPresent('div.k-spreadsheet-data > div:nth-child(16)', 'k-spreadsheet-haxis', 'gridlines is present')
          .enableToggleButton("Formula Bar")
          .verify.attributeContains(preferencePage.elements.formulaBar.selector, 'style', '', 'Formula Bar is not displayed')
          .enableToggleButton("Column Headers")
          .verify.attributeContains(preferencePage.elements.columnHeader.selector, 'style', 'height: 20px;', 'Column header is not displayed')
          .enableToggleButton("Sheet Bar")
          .verify.attributeContains(preferencePage.elements.sheetBar.selector, 'style', '', 'Sheet Bar is not displayed')
          .enableToggleButton("Row Headers")
          .verify.attributeContains(preferencePage.elements.rowHeader.selector, 'style', 'width: 32px;', 'Row header is not displayed')
          .enableToggleButton("Gridlines")
          client.verify.not.cssClassPresent('div.k-spreadsheet-data > div:nth-child(16)', 'k-spreadsheet-haxis', 'gridlines is not present')
        dashBoardPage
          .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          .click(properties.get("nextButton"))
      })

      .waitUntilLoaderPresent(function () {
        client1 = client
        previewPublishPage
          .verify.containsText(properties.get("activeTabText"), "Publish", "Publish Tab is not active")
          .validatePublishCheckpoints()
          .openPreview()
          .waitForElementPresent(dashBoardPage.elements.previewActiveCellData.selector, 10000)
          .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          .verify.containsText(dashBoardPage.elements.previewActiveCellData.selector, 'Cruise', 'Text is not visible')
		  .pause(20000)
          .closePreview()
          .pause(10000)
          .publishItem()
          .api.useXpath()
          .pause(20000)
          .verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
         // .verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
          .useCss();
        getItemID

          .getLeonardoID(client)
          .then(function (value) {
            logger.info("Item ID: " + value);
            ItemId = value
          })
      })

      .click(properties.get("finishButton"))
      .waitUntilLoaderPresent(function () {
        dashBoardPage
          .openItemPreview(ItemId)
          .waitForElementPresent(dashBoardPage.elements.previewActiveCellData.selector, 10000)
          .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          .verify.containsText(dashBoardPage.elements.previewActiveCellData.selector, 'Cruise', 'Text is not visible');

        preferencePage
          .verify.attributeContains(preferencePage.elements.formulaBar.selector, 'style', '', 'Formula Bar is not displayed')
          .verify.attributeContains(preferencePage.elements.columnHeader.selector, 'style', 'height: 20px;', 'Column header is not displayed')
          .verify.attributeContains(preferencePage.elements.sheetBar.selector, 'style', '', 'Sheet Bar is not displayed')
          .verify.attributeContains(preferencePage.elements.rowHeader.selector, 'style', 'width: 32px;', 'Row header is not displayed')
          client.verify.not.cssClassPresent('div.k-spreadsheet-data > div:nth-child(16)', 'k-spreadsheet-haxis', 'gridlines is not present')

        previewPublishPage
          .closePreview()
      })

     /* .perform(function () {
        dashBoardPage
          .openEmbed(ItemId)
        logger.info("Launching the Leonardo Paint Embed Item")
        getEmbedID
          .getEmbedUrl(client)

          .then(function (value) {
            logger.info("Embed Item ID: " + value);
            embedurl = value;
          })
      })
     .waitUntilLoaderPresent(function () {

        documentUpload
          .closePreviewDocument()
        client.url(embedurl, function () {
          logger.info("Validate the Leonardo Embed Item")
          client.waitForElementVisible('div.leo-widget', 20000)
          dashBoardPage
            .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
            .verify.containsText(dashBoardPage.elements.previewActiveCellData.selector, 'Cruise', 'Text is not visible')
            .verify.attributeContains('.k-spreadsheet-action-bar', 'style', '', 'Formula Bar is not displayed')
            .verify.attributeContains('.k-spreadsheet-column-header', 'style', 'height: 20px;', 'Column header is not displayed')
            .verify.attributeContains('.k-spreadsheet-sheets-bar', 'style', '', 'Sheet Bar is not displayed')
            .verify.attributeContains('.k-spreadsheet-row-header', 'style', 'width: 32px;', 'Row header is not displayed')
            client.verify.not.cssClassPresent('div.k-spreadsheet-data > div:nth-child(16)', 'k-spreadsheet-haxis', 'gridlines is not present')
        })
      })*/
      })

  //}),


    it('TC02: Validate "Create Presentation Item" workflow for a workbook having two Sheets', function (client) {
      logger.info("Launching the Leonardo Paint")
      var Title = "leonardo-Mulitple Sheet-Presentation-Test"
      client
        .url(envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url)
        .waitUntilLoaderPresent(function () {
          createItem
            .openCreate()
            .verify.containsText(createItem.elements.createPageHeader.selector, 'Create a New Leonardo Item', 'Unable to open Create page')
            .verify.cssClassPresent(createItem.elements.presetationItem.selector, 'modeItem', 'Presentation Item is Active')
            .createItem(Title, "presentation")
        })

        .waitUntilLoaderPresent(function () {
          setupPage
            .verify.containsText(properties.get("activeTabText"), "Setup", "SetUp Tab is not active")
            .verify.value('#title', Title, 'SetUp Page Title is not Correct')
            .enterSetupDetails("Description", "Other Tag")
            .verify.containsText('#tag-sel-ctn-2', "Other Tag", "Tag not entered correctly")
            .click(properties.get("nextButton"))
        })

        .waitUntilLoaderPresent(function () {
          documentUpload
            .verify.containsText(properties.get("activeTabText"), "Document", "Documents Tab is not active")
            .uploadDocument('MultiSheet_Final.xlsx')
            .verify.containsText(documentUpload.elements.submissionStatus1.selector, String("Uploaded on").trim())
            .pause(10000)
            .openPreviewDocument('1')
            .verify.containsText(documentUpload.elements.previewWindowHeader.selector, 'Presentation Document')
            .verify.containsText(documentUpload.elements.activeCellData.selector, '500')
          dashBoardPage
            .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          documentUpload
            .closePreviewDocument()
            .click(properties.get("nextButton"))
        })

        .waitUntilLoaderPresent(function () {

          preferencePage
            .verify.containsText(properties.get("activeTabText"), "Preference", "Preference Tab is not active")
            .waitForElementVisible('div.leo-presentationwidget', 20000)
            .selectSheet("All Sheets")
            .verify.containsText(preferencePage.elements.activeSheet.selector, "All Sheets", "Sheet is not selected coprrectly")
            .enableToggleButton("All")
            .verify.attributeContains(preferencePage.elements.ribbon.selector, 'style', 'position: relative;', 'Ribbon is not present')
            .verify.attributeContains(preferencePage.elements.formulaBar.selector, 'style', '', 'Formula Bar is not displayed')
            .verify.attributeContains(preferencePage.elements.sheetBar.selector, 'style', '', 'Sheet Bar is not displayed')
            .verify.attributeContains(preferencePage.elements.rowHeader.selector, 'style', 'width: 32px;', 'Row header is not displayed')
            .verify.attributeContains(preferencePage.elements.columnHeader.selector, 'style', 'height: 20px;', 'Column header is not displayed')
            client.verify.not.cssClassPresent('div.k-spreadsheet-data > div:nth-child(5)', 'k-spreadsheet-haxis', 'gridlines is  present')
		preferencePage
            .enableToggleButton("All")
            .verify.attributeContains(preferencePage.elements.ribbon.selector, 'style', 'position: relative; display: none;', 'Ribbon is present')
            .verify.attributeContains(preferencePage.elements.formulaBar.selector, 'style', 'display: none;', 'Formula Bar is  displayed')
            .verify.attributeContains(preferencePage.elements.sheetBar.selector, 'style', '', 'Sheet Bar is  displayed')
            .verify.attributeContains(preferencePage.elements.rowHeader.selector, 'style', 'width: 0px;', 'Row header is  displayed')
            .verify.attributeContains(preferencePage.elements.columnHeader.selector, 'style', 'height: 0px;', 'Column header is  displayed')
          //  .verify.cssClassPresent('div.k-spreadsheet-data > div:nth-child(5)', 'k-spreadsheet-vaxis', 'gridlines is  not present')
          dashBoardPage
            .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          //  preferencePage  .selectSheet("Sheet2")
            .click(properties.get("nextButton"))
        })

        .waitUntilLoaderPresent(function () {
          previewPublishPage
            .verify.containsText(properties.get("activeTabText"), "Publish", "Publish Tab is not active")
            .validatePublishCheckpoints()
            .openPreview()
            .waitForElementPresent(dashBoardPage.elements.previewActiveCellData.selector, 10000)
            .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
            .verify.containsText(dashBoardPage.elements.previewActiveCellData.selector, '500', 'Text is not visible')
            .pause(20000)
          .closePreview()
          .pause(10000)
          .publishItem()
          .api.useXpath()
          .pause(20000)
            .verify.containsText(previewPublishPage.elements.itemState.selector, "Published", "Item failed to publish")
        //    .verify.containsText(previewPublishPage.elements.publishedID.selector, "leo-leonardo-dev", "Item failed to publish")
            .useCss();

          getItemID

            .getLeonardoID(client)
            .then(function (value) {
              logger.info("Item ID: " + value);
              ItemId = value
            })
        })

        .click(properties.get("finishButton"))
        .waitUntilLoaderPresent(function () {
          dashBoardPage
            .openItemPreview(ItemId)
            .waitForElementPresent(dashBoardPage.elements.previewActiveCellData.selector, 10000)
            .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
            .verify.containsText(dashBoardPage.elements.previewActiveCellData.selector, '500', 'Text is not visible');
          preferencePage
            .verify.cssClassPresent(preferencePage.elements.ribbon.selector, 'leonardoRibbon', 'Ribbon is present')
            .verify.attributeContains(preferencePage.elements.formulaBar.selector, 'style', 'display: none;', 'Formula Bar is  displayed')
            .verify.attributeContains(preferencePage.elements.sheetBar.selector, 'style', '', 'Sheet Bar is  displayed')
            .verify.attributeContains(preferencePage.elements.rowHeader.selector, 'style', 'width: 0px;', 'Row header is  displayed')
            .verify.attributeContains(preferencePage.elements.columnHeader.selector, 'style', 'height: 0px;', 'Row header is  displayed')
            .verify.cssClassPresent('div.k-spreadsheet-data > div:nth-child(5)', 'k-spreadsheet-haxis', 'gridlines is  present')
          previewPublishPage
            .closePreview()
        })

       /* .perform(function () {
          dashBoardPage
            .openEmbed(ItemId)
          logger.info("Launching the Leonardo Paint Embed Item")
          getEmbedID
            .getEmbedUrl(client)

            .then(function (value) {
              logger.info("Embed Item ID: " + value);
              embedurl = value;
            })
        })
        .waitUntilLoaderPresent(function () {

          documentUpload
            .closePreviewDocument()
          client.url(embedurl, function () {
            logger.info("Validate the Leonardo Embed Item")
            client.waitForElementVisible('div.leo-widget', 20000)

              .verify.not.cssClassPresent('.leo-widget.leo-presentationwidget .widgetContainer', 'ribbonAvailable', 'Ribbon is present')
              .verify.attributeContains('.leonardoPlayerContainer .k-spreadsheet-action-bar', 'style', 'display: none;', 'Formula Bar is  displayed')
              .verify.attributeContains('.leonardoPlayerContainer .k-spreadsheet-sheets-bar', 'style', '', 'Sheet Bar is  displayed')
              .verify.attributeContains('.leonardoPlayerContainer .k-spreadsheet-row-header', 'style', 'width: 0px;', 'Row header is  displayed')
              .verify.attributeContains('.leonardoPlayerContainer .k-spreadsheet-column-header', 'style', 'height: 0px;', 'column header is  displayed')
              .verify.cssClassPresent('div.k-spreadsheet-data > div:nth-child(5)', 'k-spreadsheet-haxis', 'gridlines is  present')
            dashBoardPage
              .verify.cssClassPresent(dashBoardPage.elements.previewActiveCell.selector, 'k-state-disabled', 'Item is Editable')
          })*/

        })
  

  afterEach(function (client, done) {
    testcase_name = this.currentTest.title;
    logger.info("Completed following Test Case: " + testcase_name)
    done();
  });

  after(function (client, done) {
    client.end(function () {
      done();
    })
  })
});
