var getSelector = require(currentDirPath + '/utils/modules/getSelectorForCell');

describe('Validate the Trial Balnce Demo', function (client) {

	this.timeout(90000000);
	var testfile_name = this.file;
	var suite_name = this.title;
	var testcase_name;

	before(function (client, done) {

		logger.info("Executing File: " + testfile_name + ".js")
		logger.info("Starting following suite: " + suite_name)
		logger.info("Launching Browser: " + client.options.desiredCapabilities.browserName)
		logger.info("Launching the Leonardo Player URL: " + "http://demos.leonardodls.com/")
		embedPlayer = client.page.embedPlayer();


		client
			.maximizeWindow()
			.url("http://compro:c0mpr0@demos.leonardodls.com/")
			.waitForElementVisible('.productName', 10000, function () {
				done();
			})
	});

	beforeEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Executing following Test Case: " + testcase_name)
		done();
	});

	it('TC01 - Valdiate the Trial Balance', function (client, done) {
		var client1 = client
		client
			.click('app-dashboard-item:nth-child(1) .button-container .btn')
			.waitForElementVisible('.lr_tabbar', 10000, function () {
				client
					.selectRange("A1:C1")
					.selectElement("mergeandcenter")
					.selectElement("bold")
					.selectCell("A6")
					.selectCell("A1")
					.enterText("Fantasy Group")
					.pause(1000)
					.selectRange("A2:C2")
					.selectElement("mergeandcenter")
					.selectElement("bold")
					.selectCell("A6")
					.selectCell("A2")
					.enterText("Trial Balance")
					.pause(1000)
					.selectRange("A3:C3")
					.selectElement("mergeandcenter")
					.selectElement("bold")
					.selectCell("A6")
					.selectCell("A3")
					.enterText("March 31, 2018")
					.pause(1000)
					.selectCell("A6")
					.enterText("Cash")
					.selectCell("B6")
					.enterText("17300")
					.pause(1000)
					.selectCell("A20")
					.enterText("Total")
					.pause(1000)
					.selectCell("B20")
					.enterText("=Sum(B6:B19 )")
					.pause(1000)
					.selectCell("C20")
					.enterText("=Sum(C6:C19 )")
					.pause(1000)
					.click('buttonpanel button:nth-child(1)')
					.waitForElementVisible('#myModalLabel18 > button', 10000, function () {
						client
							.click('#myModalLabel18 > button')
							.pause(1000)
							.click('button:nth-child(2)')
							.click('button:nth-child(2)')
							.click('button:nth-child(2)')
							.click('button:nth-child(3)')
							.selectCell("B6")
							embedPlayer.validateFeedbackHeaderTextForCell("B6", "Correct")
						client	
							.verify.elementCount(embedPlayer.elements.correctCells.selector, 35)
							.click('button:nth-child(3)')
							.selectCell("B6")
							.keys(client.Keys.DELETE)
							.enterText("9230")
							.click('button:nth-child(3)')
							.verify.elementCount(embedPlayer.elements.correctCells.selector, 32)
							.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 2)
							.pause(1000)
							.selectCell("A6")
							embedPlayer.validateFeedbackHeaderTextForCell("B6", "Correct")
							client.	selectCell("A20")
							.selectCell("B6")
							embedPlayer.validateFeedbackHeaderTextForCell("B6", "Incorrect")
						client	
							.click('button:nth-child(3)')
							.selectCell("B6")
							.keys(client.Keys.DELETE)
							.selectCell("B6")
							.enterText("17300")
							.click('button:nth-child(3)')
							.verify.elementCount(embedPlayer.elements.correctCells.selector, 35)
							.pause(1000)
							.click('button.btn.btn-primary.pull-right')
							.click('button.btn.btn-primary.pull-right')


					})

			})

	});
	it('TC02 - Valdiate the Trial Balance Assessment', function (client, done) {
		var client1 = client
		client
			.waitForElementVisible('app-dashboard-item:nth-child(4) .button-container .btn', 10000)
			.click('app-dashboard-item:nth-child(4) .button-container .btn')
			.waitForElementVisible('.lr_tabbar', 10000, function () {
				client
					.click('button.btn.btn-primary.pull-right')
					.pause(1000)
					.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 35)
					.click('button.btn.btn-primary.pull-right')
			})

	});
	it('TC03 - Valdiate the Motion Profiler', function (client, done) {
		var client1 = client
		client
			.click('app-dashboard-item:nth-child(3) .button-container .btn')
			.waitForElementVisible('.workspaceContainer div.workspace', 10000, function () {
	client1
			
			.pause(1000)
			.selectCell("A1")
			.pause(1000)
			.selectCell("B4")
			.enterText("180")
			.selectCell("C3")
			.enterText("180")
			.selectCell("B5")
			.enterText("180")
			.selectCell("C5")
			.enterText("0")
			.selectCell("C4")
			.enterText("180")
			.pause(1000)
			.pause(1000)
			.selectCell("E3")
			.pause(1000)
			.enterText("=(C3-B3)/D3")
			.keyPress("\uE007")
			.selectCell("F3")
			.pause(1000)
			.enterText("=(C3+B3)*D3/2")
			.keyPress("\uE007")
			.selectCell("G3")
			.pause(1000)
			.enterText("=E3*E3*D3")
			.keyPress("\uE007")
			.pause(1000)
			.selectCell("E4")
			.enterText("=(C4-B4)/D4")
			.keyPress("\uE007")
			.selectCell("F4")
			.pause(1000)
			.enterText("=(C4+B4)*D4/2")
			.keyPress("\uE007")
			.selectCell("G4")
			.pause(1000)
			.enterText("=E4*E4*D4")
			.keyPress("\uE007")
			.selectCell("E5")
			.enterText("=(C5-B5)/D5")
			.keyPress("\uE007")
			.selectCell("F5")
			.enterText("=(C5+B5)*D5/2")
			.keyPress("\uE007")
			.selectCell("G5")
			.pause(1000)
			.enterText("=E5*E5*D5")
			.keyPress("\uE007")
			.selectCell("E6")
			.pause(1000)
			.enterText("=(C6-B6)/D6")
			.keyPress("\uE007")
			.selectCell("F6")
			.pause(1000)
			.enterText("=(C6+B6)*D6/2")
			.keyPress("\uE007")
			.selectCell("G6")
			.pause(1000)
			.enterText("=E6*E6*D6")
			.keyPress("\uE007")
			.selectCell("B9")
			.pause(1000)
			.enterText("=sqrt(sum(G3:G6)/sum(D3:D6))")
			.selectCell("A1")
			.pause(1000)
			.click('button.navBtn.btnCMW > span')
			.verify.elementCount(embedPlayer.elements.correctCells.selector, 19)
			.verify.elementCount(embedPlayer.elements.incorrectCells.selector, 0)
			
			})

	});
	afterEach(function (client, done) {
		testcase_name = this.currentTest.title;
		logger.info("Completed following Test Case: " + testcase_name)
		client.pause(1000, function () {
			done();

		})
	});

	after(function (client, done) {
		logger.info("Completed following suite : " + suite_name)
		 client.end(function () {
			done();
		}); 
	});
});