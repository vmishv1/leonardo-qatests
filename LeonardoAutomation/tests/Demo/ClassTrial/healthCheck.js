describe ('Class Trial Health Check Test', function(client) {
  
  this.timeout(900000);
        
  it('Launch Class Trial URL and Login into Class Trial Account', function(client) {
    client
      .maximizeWindow()
      .url("https://accounting-test.leonardodls.com/accounting-test?loginmode=support")
      .useXpath()
      .pause(20000)
      .verify.elementPresent('//input[@name="username"]', 'Login Page not loaded successfully')
      .setValue('//input[@name="username"]', "comproQA_Automation_Test_1")
      .setValue('//form[contains(@class, "login")]//input[@name="email"]', "comproQA_Automation_Test_1@gmail.com")
      .setValue('//select[@id = "accountingClassSelect"]', "Support Mode Class")
      .click('//div[contains(@class, "submit-btn")]')
  })
            
  it ('Validate User Name and Class and Proceed to Assignment', function(client) {
    client
      .waitForElementVisible('//button[contains(text(), "Proceed To Assignment")]/parent::div', 20000)
      .verify.containsText('//a[@id="users-drop-down"]', "comproQA_Automation_Test_1", "User not logged in successfully") 
      .verify.containsText('//p[contains(@class, "welcome-text-para")]', "Welcome comproQA_Automation_Test_1", "Welcome Text is not correct") 
      .verify.containsText('//span[contains(@class, "class-name-span")]', "Support Mode Class", "Class Name is not correct") 
      .click('//button[contains(text(), "Proceed To Assignment")]/parent::div')
      .pause(10000)
  })

  it ('Validate First Question Text and Proceed to Question 8', function(client) {
    client
      .frame(0)
      .waitForElementPresent('//div[contains(@class, "smart-form")]//p', 10000)
      .verify.containsText('//div[contains(@class, "smart-form")]//p', "In January, McMaster Company earned $17,000 in revenue and incurred $15,000 in expenses. The company generated", "Question 1 not painted correctly") 
      .verify.elementCount('//div[contains(@class, "smart-form")]//ul/li', 4, "Answer Option count not matching") 
      .frameParent()
      .click('//div[contains(@class, "ques-nav")]//span[contains(@class, "ques-inst")][8]')
      .waitForElementNotVisible('//div[@id="assessmentComponentLoader"]', 20000)
  })

  it ('Validate Question 8 Grid Cell Text and Proceed to Instructions Page', function(client) {
    client
      .frame(0)
      .pause(5000)
      .useCss()
      .selectCell('A1')
      .useXpath()
      .verify.containsText('//div[contains(@class, "leo-canvasarea")]//div[contains(@class, "k-spreadsheet-active-cell")]//span', 'State-to-State Consulting')    
      .frameParent()
      .click('//span[contains(text(), "Instructions")]//ancestor::div[contains(@role, "tab")][1]')
      .waitForElementVisible('//button[contains(text(), "Proceed To Assignment")]/parent::div', 10000)
      .pause(5000)  
  })
  
  it ('Exit from Class Trial', function(client) {
    client
      .click('//a[@id="users-drop-down"]')
      .waitForElementVisible('//a[contains(text(), "Exit Assignment")]', 5000)
      .click('//a[contains(text(), "Exit Assignment")]')   
      .pause(10000)
      .verify.elementPresent('//input[@name="username"]', 'Login Page not loaded successfully after exiting application')
  })

  after(function(client, done) {
    client.end(function() {
      done();
    });
  });
})
    
 

  
