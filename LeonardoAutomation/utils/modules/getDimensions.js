'use strict'
//getDimensions
exports.getSubItemHeight = function(rowNum, subItem, self) {

    // var self = this;
    var hgt;
    return new Promise(function(resolve, reject) {
        //console.log("logging height");
        var rowPath = "div.leo-instructionarea div.instruction-spreadsheet-component:nth-of-type(" + subItem + ") > div.widgetContainer > div.spreadsheet.k-widget.k-spreadsheet > div.k-spreadsheet-view > div.k-spreadsheet-fixed-container > div.k-spreadsheet-pane.k-top.k-left > div.k-spreadsheet-row-header > div:nth-child(" + rowNum + ") > div"
            
        self.api.getElementSize(rowPath, function(result) {
            hgt = (result.value.height) / 2;
        })
        self.api.getLocationInView(rowPath, function(result) {
            //hgt = hgt / 2 + result.value.y;
			hgt = hgt + result.value.y;
            resolve(hgt);
        })
    })
}

exports.getSubItemWidth = function(colNum, subItem, self) {

    // var self = this;
    var wdth;
    return new Promise(function(resolve, reject) {
       // console.log("calculating Width");
        var colpath = "div.leo-instructionarea div.instruction-spreadsheet-component:nth-of-type(" + subItem + ") > div.widgetContainer > div.spreadsheet.k-widget.k-spreadsheet > div.k-spreadsheet-view > div.k-spreadsheet-fixed-container > div.k-spreadsheet-pane.k-top.k-left > div.k-spreadsheet-column-header > div:nth-child(" + colNum + ") > div"
        self.api.getElementSize(colpath, function(result) {
            wdth = (result.value.width) / 2;
        })

        self.api.getLocationInView(colpath, function(result) {
            //wdth = wdth / 2 + result.value.x;
			wdth = wdth + result.value.x;
            resolve(wdth);
        })
    })

}

exports.getRowHeightOffset = function(rowNum,self) {
    // var self = this;
    var hgt;
    var rowPath;
	rowPath= "//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-row-header')]//*[contains(@class,'k-vertical-align-center') and text()='"+rowNum+"']/.."
    return new Promise(function(resolve, reject) {
		//self.api.useXpath(function(){
			self.api.getElementSize("xpath",rowPath, function(result) {
				hgt = (result.value.height) / 2;
				//self.api.getLocationInView("xpath",rowPath, function(result) {
				self.api.getLocation("xpath",rowPath, function(result) {
					hgt = hgt + result.value.y;
					//self.api.useCss()
					if(self.api.options.browserName == "firefox")
					{
						self.api.getElementSize("body",function(result){
							hgt = hgt - (result.value.height/2);
							//console.log("height=" + hgt);
							resolve(hgt);
							
						})
		
					}
					else
					{
						
						resolve(hgt);
					}
				})
			})
		
    })
}

exports.getColWidthOffset = function(colNum,self) {
    // var self = this;
    var wdth;
    var colpath;
	colpath ="//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-column-header')]//*[contains(@class,'k-vertical-align-center')and text()='"+colNum+"']/.."
    return new Promise(function(resolve, reject) {
		//self.api.useXpath(function(){
			self.api.getElementSize("xpath",colpath, function(result) {
				wdth = (result.value.width) / 2;
				//self.api.getLocationInView("xpath",colpath, function(result) {
				self.api.getLocation("xpath",colpath, function(result) {
					wdth = wdth + result.value.x;
					//self.api.useCss()
					if(self.api.options.browserName == "firefox")
					{
						self.api.getElementSize("body",function(result){
							wdth = wdth - (result.value.width / 2);
							//console.log("width=" + wdth);
							resolve(wdth);
							
						})
		
					}
					else
					{
						//console.log("width" =wdth);
						resolve(wdth);
					}
					
				})
			})
		//})
    })
}

exports.getRowColRef = function(cellName,self) {
    var colName = cellName.replace(/[^a-z]/gi, '');
    var rowNum = cellName.replace(/[^0-9]/g, '');
    var colNum = cellName.charCodeAt(0);
    //console.log("CHARCTER CODE FOR POSITION 0 IS: "+colNum)
    var colNum1 = cellName.charCodeAt(1);
    //console.log("CHARCTER CODE FOR POSITION 1 IS: "+colNum1)
    // change alphabet to index number
    if (colNum1 <= 57 && colNum1 >= 49){  
        //console.log("EXECUTING FOR SINGLE CHARACTER COLUMN NAME")    
        colNum = colNum - 64;
        //console.log("FINAL COLUMN NUMBER IS: "+colNum)
    }
    else {
        //console.log("EXECUTING FOR DOUBLE CHARACTER COLUMN NAME")
        colNum=((colNum- 64)*26)+(colNum1-64);
    }
    return {
        col:colName,
        row:rowNum,
        colNo:colNum
    }
    
}

