var util = require('util');
var events = require('events');
var fs = require('fs');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function dragAndDrop() {
    events.EventEmitter.call(this);
}


util.inherits(dragAndDrop, events.EventEmitter);

dragAndDrop.prototype.command = function (OriginElementName, origin_x, origin_y, TargetElementName, target_x, target_y, preEvaluation, callback) {
    var self = this; 
	var promise3 = new Promise(function(resolve,reject)	{
		origin_x_offset= calc_x_percent(self,properties.get(OriginElementName),origin_x);
		origin_y_offset= calc_y_percent(self,properties.get(OriginElementName),origin_y);
		target_x_offset= calc_x_percent(self,properties.get(TargetElementName),target_x);
		target_y_offset= calc_y_percent(self,properties.get(TargetElementName),target_y);
		Promise.all([origin_x_offset,origin_y_offset,target_x_offset,target_y_offset]).then(function(values) {
			resolve(values);
		});
	});

	var promise1 = new Promise(function(resolve, reject) {
		self.api.getLocation(properties.get(OriginElementName),function (result)	{
			if(typeof result !== 'undefined')
			resolve(result);
		})
	});

	var promise2 = new Promise(function(resolve, reject) {
		self.api.pause(300);
		self.api.getLocation(properties.get(TargetElementName),function (result)	{  
			if(typeof result !== 'undefined')
			resolve(result);
		})
	});

	async function execute()	{
		Promise.all([promise1, promise2,promise3]).then(function(values) {
			coordinateorigin=values[0];
			coordinatetarget=values[1];
			offset = values[2];
			self.api.moveToElement("body", parseInt(coordinateorigin.value.x+offset[0]), parseInt(coordinateorigin.value.y+offset[1]))
			.pause(100)
			.mouseButtonDown(0)
			.pause(100)
			.moveToElement("body", parseInt(coordinatetarget.value.x+offset[2]),parseInt(coordinatetarget.value.y+offset[3]))
			.pause(100)
			.mouseButtonUp(0)
			.pause(100, function (result) {
				if (typeof (callback) === "function") {
					callback.call(self.client.api);
					self.emit('complete');
				}
			})
		});
		return this;
	}
	
	function calc_x_percent(self,element,value)	{   
		return new Promise(function(resolve,reject)	{
			var patt = /\%/;
			if (patt.test(value)) {
				value = value.split("%")[0];
				value = Number(value);
				self.api.getElementSize(element,function(result)	{ 
					resolve((result.value.width/100)*value);
				});
			}
			else	{
			  resolve(Number(value));
			}
		})
	}

	function calc_y_percent(self,element,value)	{
		return new Promise(function(resolve,reject)	{
			var patt = /\%/;
			if (patt.test(value)) {
				value = value.split("%")[0];
				value = Number(value);
				self.api.getElementSize(element,function(result)	{ 
					resolve((result.value.height/100)*value);
				});
			}
			else	{
			  resolve(Number(value));
			}
		})		
	}
	
	execute();
}


module.exports = dragAndDrop;