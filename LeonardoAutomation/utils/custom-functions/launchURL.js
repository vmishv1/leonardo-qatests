var fs = require('fs');

exports.command = function (...params) {
    if (params.length == 3) {
        var inputData = params[0]
        var client = params[1]
        var done = params[2] 
    } else {
        var client = params[0]
        var done = params[1] 
    }
    
    if (argv.appType.toLowerCase() == "paint") {
    
    	loginPage = client.page.loginPage();
	    if (argv.testEnv.toLowerCase() == "qa" || argv.testEnv.toLowerCase() == "staging" || argv.testEnv.toLowerCase() == "production") {
	    	var url = envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url
	    } else {
	    	var url = argv.testEnv
	    }
	    if (!argv.org) {
	    	argv.org = envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].org
	    }
	    if (!argv.username) {
	    	argv.username = envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].username
	    }
	    if (!argv.password) {
	    	argv.password =	envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].password
	    } 
	    client
    		.maximizeWindow()
            .url(url, function() {
            	loginPage.loginWithDetails(argv.org, argv.username, argv.password)
            })
            .waitUntilLoaderPresent(function() {
                done();
          	})	

    
    } else if (argv.appType.toLowerCase() == "player") {


    } else if (argv.appType.toLowerCase() == "itemrenderer") {
        var itemRendererTestData = JSON.parse(fs.readFileSync('./testdata/itemRendererTestData.json'));
        // Extracting Embed ID or Item JSON path based on test environment passed and test file 
        var inputKey = itemRendererTestData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()][inputData]
        console.log(inputKey)
    	
        if (argv.testEnv.toLowerCase() == "testbench") {
    		itemPlayerTB = client.page.itemPlayerTestBench();
    		itemPlayerTBConfig = envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()];
            if(argv.name) {
                itemPlayerTBConfig.name = argv.name
            }
            if(argv.ver) {
                itemPlayerTBConfig.version = argv.ver
            }
            if(argv.width) {
                itemPlayerTBConfig.containerSettings.width = argv.width
            }
            if(argv.height) {
                itemPlayerTBConfig.containerSettings.height = argv.height
            }
            if(argv.itemJSON) {
                itemPlayerTBConfig.itemJSON = argv.itemJSON
            } else {
                itemPlayerTBConfig.itemJSON = inputKey
            }
    		client
	    		//.maximizeWindow()
				.resizeWindow(1900,1080)
	    		.url(envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url)
	            .waitForElementVisible("#itemJson", 10000, function() {
	            	itemPlayerTB
	            		.applyConfigParams(itemPlayerTBConfig)
						    client.click('div.tooglebar > button',function() {
                                client.getAttribute("xpath", '//div[@l-player-version]','l-player-version', function(versionInfo) {
                                    logger.info("Version Info is " + versionInfo.value)
                                    done();
                                })
							})
	     	    }, "ERROR!! %s did not load in %d ms.")
    	} else if (argv.testEnv.toLowerCase() == "qa" || argv.testEnv.toLowerCase() == "staging" || argv.testEnv.toLowerCase() == "production") {
            client
                .resizeWindow(1900,1080)
                .url(envData[argv.appType.toLowerCase()][argv.testEnv.toLowerCase()].url + inputKey)
                .waitForElementPresent('div.leo-canvasarea', 25000, function() {
                    client.getAttribute('xpath', '//div[@l-player-version]','l-player-version', function(versionInfo) {
                        logger.info("Version Info is " + versionInfo.value)
                        done();
                    })
                
            })
        } else {
        	client
                .maximizeWindow()
                .url(argv.testEnv)
                .waitForElementPresent('div.leo-canvasarea', 15000, function() {
                done();
            })
        }
    }
    
}