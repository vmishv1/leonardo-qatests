var util = require('util');
var events = require('events');
var fs = require('fs');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function validateElement() {
    events.EventEmitter.call(this);
    //this.timeoutInMilliseconds = 9999999999;
}

util.inherits(validateElement, events.EventEmitter);

validateElement.prototype.command = function (elementName, target, property, expected, precheckEvaluation, callback) {

    //timeoutInMilliseconds = 2000;

    var self = this;
    //var functionToExecute = "self.api." + propertyValidator + "('" + elementSelector + "','" + attribute + "', function(result){return self.api.verify.equal(result.value,'" + expectedValue + "')});";
	
	//console.log("Validation starts for " + target + " of " + elementName);
	
	if (target === "getCssProperty") {

		self.api.getCssProperty(properties.get(elementName), property, function (result) {
			console.log("Validate CSS Property of " + elementName);
			console.log("CSS Property Value Found:" + result.value);
			console.log("CSS Property Value Expected:" + expected);
			self.api.verify.equal(result.value, expected);
			if (typeof (callback) === "function") {
				callback.call(self.client.api);
				//console.log("Validation ends for " + target + " of " + elementName);
			}
			self.emit('complete');
			return this;
		});
	}

	else if (target === "getAttribute"){

		self.api.getAttribute(properties.get(elementName), property, function (result) {
			console.log("Validate Attribute Value of " + elementName);
			console.log("Attribute Value Found:" + result.value);
			console.log("Attribute Value Expected:" + expected);
			self.api.verify.equal(result.value, expected);
			if (typeof (callback) === "function") {
				callback.call(self.client.api);
				//console.log("Validation ends for " + target + " of " + elementName);
			}
			self.emit('complete');
			return this;
		});
	}

	else if (target === 'getElementPropertyValue') {
        self.api.getValue(properties.get(elementName), function(result) {
			console.log("Validate Element Property Value of " + elementName);
			console.log("Element Property Value Found:" + result.value);
            console.log("Element Property Value Expected:" + expected);
            self.api.verify.equal(result.value, expected);
            if (typeof(callback) === "function") {
                callback.call(self.client.api);
				//console.log("Validation ends for " + target + " of " + elementName);
            }
            self.emit('complete');
            return this;
        })
    }
	
	else if (target === 'getText') {
        self.api.getText(properties.get(elementName), function(result) {
			console.log("Validate Element Text of " + elementName);
			console.log("Element Text Found:" + result.value);
            console.log("Element Text Expected:" + expected);
            self.api.verify.equal(result.value, expected);
            if (typeof(callback) === "function") {
                callback.call(self.client.api);
				//console.log("Validation ends for " + target + " of " + elementName);
            }
            self.emit('complete');
            return this;
        })
    }

	else {
		console.log("ERROR!! Incorrect target provided in json: " + target);
		self.api.verify.equal(-1,0,"ERROR!! Incorrect target provided: "+ target);		
	}
	
	//console.log("Validation ends for " + target + " of " + elementName);

};

module.exports = validateElement;