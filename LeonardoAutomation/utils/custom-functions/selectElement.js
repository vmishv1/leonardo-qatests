
var util = require('util');
var events = require('events');
var fs = require('fs');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function selectElement() {
	
events.EventEmitter.call(this);

}

util.inherits(selectElement, events.EventEmitter);

selectElement.prototype.command = function(propertyName, callback) {

    var self = this;
	self.api
			.waitForElementVisible(properties.get(propertyName), 5000, 100, true, function() {}, "!!ERROR: Element %s was not in the page for %d ms")
			.click(properties.get(propertyName),function() {
				//self.api.pause((100),function() {
					if (typeof (callback) === "function") {
						callback.call(self.client.api);
					}
					self.emit('complete');
					return this;
				//})
			})
};

module.exports = selectElement;
