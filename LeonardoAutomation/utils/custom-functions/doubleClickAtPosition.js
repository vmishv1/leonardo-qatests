var util = require('util');
var events = require('events');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");


function doubleClickAtPosition() {
    events.EventEmitter.call(this);
}

util.inherits(doubleClickAtPosition, events.EventEmitter);

doubleClickAtPosition.prototype.command = function(elementName, cb) {

    var self = this;
    if(elementName == undefined) {
        self.api.doubleClick(function() {
            cb.call(self.client.api)
            self.emit('complete');
            return this;
        });
    }
    else {
        self.api.moveToElement(properties.get(elementName), undefined, undefined, function() {
            self.api.doubleClick(function() {
                cb.call(self.client.api)
                self.emit('complete');
                return this;
            })   
        })
    }
}
module.exports = doubleClickAtPosition;