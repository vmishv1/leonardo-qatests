var util = require('util');
var events = require('events');
var syncLoop = require('../modules/syncLoop.js')


function getFormattingData() {
	events.EventEmitter.call(this);
}


util.inherits(getFormattingData, events.EventEmitter);


getFormattingData.prototype.command = function (rowIndex, cb) {
	var self = this;
	var sheetName, cellName, prop, propValue, opJson;
	let array = [],
		properties = [];

	self.api.element('css selector', 'div:nth-child(' + rowIndex + ') > cell-handler a.showDetails.down', function (number) {
		self.api.getText('div:nth-child(' + rowIndex + ') > cell-handler span.cell', function (result) {
			cellName = result.value;
			self.api.getText('div:nth-child(' + rowIndex + ') > cell-handler span.badge-sheet', function (result) {
				sheetName = result.value;
			})
		})
		if (number.status === -1) {
			self.api.getText('div:nth-child(' + rowIndex + ')  > cell-handler div.description.text-left > div.d-flex.description-content > span:nth-child(2)', function (result) {
				propValue = result.value;
				self.api.getText('div:nth-child(' + rowIndex + ') > cell-handler > div > div.description.text-left > div:nth-child(2)', function (result) {
					prop = (result.value).substr(10);
					properties.push({
						"propName": prop,
						"propValue": propValue
					})
					array.push({
						"sheetName": sheetName,
						"cellName": cellName,
						"properties": properties
					})
					opJson = {
						"Cells": array,
					}
					cb(opJson);
					self.emit('complete');
				})
			})
		}
		else {
			var x = 2;
			self.api.click('div:nth-child(' + rowIndex + ') > cell-handler a.showDetails.down', function () {
				self.api.elements('css selector', 'div:nth-child(' + rowIndex + ') > cell-handler span.validation', function (result) {
					syncLoop.loopExecution(result.value.length, function (loop) {
						self.api.waitForElementPresent('div:nth-child(' + rowIndex + ') > cell-handler div.toggle-rule > i', 10000, function () {
						})
							.getText('div:nth-child(' + rowIndex + ') > cell-handler div:nth-child(' + x + ') > div.validationString.text-left > span.validation', function (result) {
								prop = result.value;
							})
							.getText('div:nth-child(' + rowIndex + ') > cell-handler div:nth-child(' + x + ') > div.validationString.text-left > span.value', function (result) {
								propValue = result.value
								properties.push({
									"propName": prop,
									"propValue": propValue
								})
								x++;
								loop.next();
							})
					}, function () {
						array.push({
							"sheetName": sheetName,
							"cellName": cellName,
							"properties": properties
						})
						opJson = {
							"Cells": array,
						}
						cb(opJson);
						self.emit('complete');
					})
				})
			});
		}
	});
};

module.exports = getFormattingData;