var util = require('util');
var events = require('events');

var calculateDimensions = require('./../modules/getDimensions');

function selectCell() {
    events.EventEmitter.call(this);
}


util.inherits(selectCell, events.EventEmitter);


selectCell.prototype.command = function(cellName, cb) {

    var obj = calculateDimensions.getRowColRef(cellName)
    var rowNum = obj.row
    var colNum = obj.colNo
    var colName = obj.col
    var self = this;
    var element;

    element = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1);
    
    async function selectingCell() {
        var newWidth = await calculateDimensions.getColWidthOffset(colName, self);
        var newHeight = await calculateDimensions.getRowHeightOffset(rowNum, self);
        // console.log(newWidth + " -- " + newHeight);
		self.api
			.moveToElement("body", parseInt(newWidth), parseInt(newHeight))
            .mouseButtonClick(0, function() {
                if (typeof(cb) === "function") {
                    cb.call(self.client.api);
                }
                self.emit('complete');
            })
    }

    self.api.element("css selector", element, function(result) {
        //if the css class of the corresponding cell does not exist call the original select cell criteria
        if (typeof(result.value.ELEMENT) == "undefined") {
            selectingCell();
            return this;
        }
		else {
			self.api
					//.waitForElementVisible(element, 5000, 100, true, function() {}, "!!ERROR: Cell " + cellName + " was not visible within %d ms.")
					//.waitForElementVisible(element, 5000, 500)
					.click("css selector",element, function() {
						if (typeof(cb) === "function") {
							cb.call(self.client.api);
						}
						self.emit('complete');
						return this;
            })
        }
    });
};

module.exports = selectCell;