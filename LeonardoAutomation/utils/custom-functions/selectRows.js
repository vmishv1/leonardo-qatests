var util = require('util');
var events = require('events');
var fs = require('fs');

function selectRows() {

    events.EventEmitter.call(this);
}

util.inherits(selectRows, events.EventEmitter);

selectRows.prototype.command = function(range, cb) {

    var cellRange = String(range).split(":");
    var self = this;
	var startrow = "//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-row-header')]//div[text()='"+cellRange[0]+"']/.."
	var endrow = "//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-row-header')]//div[text()='"+cellRange[1]+"']/.."
	self.api.moveToElement("xpath", startrow, undefined, undefined)
			.mouseButtonDown(0)
			.pause(200)
			.moveToElement("xpath", endrow, undefined, undefined)
			.mouseButtonUp(0, function() {
				if (typeof(cb) === "function") {
					cb.call(self.client.api);
				}
				self.emit('complete');
				return this;
	})
};

module.exports = selectRows;