var util = require('util');
var events = require('events');
var calculateDimensions = require('./../modules/getDimensions');

function validateBorder() {
    events.EventEmitter.call(this);
}


util.inherits(validateBorder, events.EventEmitter);

validateBorder.prototype.command = function(cellRef, target, cb) {
    var self = this; 
    //console.log("inside validateBorder function")
    var elementSelectorStatus, destCellElementSelectorStatus;
    var elementSelector,destCellElementSelector;

    if (String(cellRef).includes(":")) {
        //console.log("CELL RANGE: "+cellRef);
        var cellRange = String(cellRef).split(":");
        //console.log("Cell Range after splitting:" +cellRange)
        
        var sourceCell = cellRange[0]
        //console.log("SOURCE CELL: "+sourceCell);
        var destCell = cellRange[1]
        //console.log("DEST CELL: "+destCell);

        //source cell element selector
        var obj= calculateDimensions.getRowColRef(sourceCell)
        //console.log("obj got from getDimension is: "+obj)
        var colNum = obj.colNo
        //console.log("colNum got from getDimension is: "+colNum)
        var rowNum = obj.row
        //console.log("rowNum got from getDimension is: "+rowNum)
        var elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1);
        //console.log("Element selctor for Source cell is: "+elementSelector)
    
        //destination cell element selector
        var obj1= calculateDimensions.getRowColRef(destCell)
        //console.log("obj1 got from getDimension is: "+obj1)
        var destcolNum = obj1.colNo;
        //console.log("colNum got from getDimension is: "+destcolNum)
        var desrowNum = obj1.row
        //console.log("rowNum got from getDimension is: "+desrowNum)
        var destCellElementSelector = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (desrowNum - 1) + ".k-col-" + (destcolNum - 1);
        //console.log("Element selctor for Destination cell is: "+destCellElementSelector)
    }
    else {
        //console.log("executing else block")
        var obj= calculateDimensions.getRowColRef(cellRef)
        //console.log("obj got from getDimension is: "+obj)
        var colNum = obj.colNo
        //console.log("colNum got from getDimension is: "+colNum)
        var rowNum = obj.row
        //console.log("rowNum got from getDimension is: "+rowNum)
        elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1);
        //console.log("element selector generated is: "+elementSelector)
        destCellElementSelector = "div.leo-canvasarea div.k-spreadsheet-cell.k-row-" + (rowNum - 1) + ".k-col-" + (colNum - 1);
        //console.log("destination element selector generated is: "+destCellElementSelector)
	}
	
	// console.log("OUTSIDE ELSE BLOCK: element selector generated is: "+elementSelector)
	self.api.element('css selector', elementSelector, function(result) {
		elementSelectorStatus = result.status;
		// console.log("OUTSIDE IF ELSE BLOCK: element selector status:"+elementSelectorStatus)
		if (0 == elementSelectorStatus) {
			self.api.element('css selector', destCellElementSelector, function(result1) {
				destCellElementSelectorStatus = result1.status;
				// console.log("OUTSIDE IF ELSE BLOCK: destination element selector status:"+destCellElementSelectorStatus)
				if (0 == destCellElementSelectorStatus)
					execute();
				else {
					console.log("ERROR!! Destination Cell not found with selector: " + destCellElementSelector)
					self.api.verify.equal(-1,0,"ERROR!! Destination Cell selection not found")  
				}
			})
		}
		else {
			console.log("ERROR!! Source Cell not found with selector: " + elementSelector)
			self.api.verify.equal(-1,0,"ERROR!! Source Cell selection not found") 
		}
	})
    
	var promise1 = new Promise(function(resolve,reject)	{
		self.api.getCssProperty(elementSelector, 'top', function(result) {
			//console.log("Top of cell is: "+ result.value);
			cellTop = result.value.replace(/[^0-9]/g, '')
			//console.log("Top of cell is: "+ cellTop);
			resolve(cellTop);
		});
	});

	var promise2 = new Promise(function(resolve, reject) {
		self.api.getCssProperty(destCellElementSelector, 'top', function(result) {
			//console.log("Bottom of cell is: "+ result.value);
			cellTop = result.value;
			//console.log("cell Bottom value is:",cellTop)
			self.api.getCssProperty(destCellElementSelector, 'height', function(result2) {
				//console.log("Cell height is: "+ result2.value);
				cellBottom = Number(result2.value.replace(/[^0-9]/g, '')) + Number(cellTop.replace(/[^0-9]/g, ''))
				//console.log("Bottom of cell is at: "+cellBottom)
				resolve(cellBottom)
			})
		})
	});
	
	var promise3 = new Promise(function(resolve, reject) {
		self.api.getCssProperty(elementSelector, 'left', function(result1) {
			//console.log("Left of cell is: "+ result1.value);
			cellLeft = result1.value.replace(/[^0-9]/g, '')
			//console.log("Left of cell is: "+ cellLeft);
			resolve(cellLeft)
		})
	});
	
	var promise4 = new Promise(function(resolve, reject) {
		self.api.getCssProperty(destCellElementSelector, 'left', function(result1) {
			//console.log("Left of cell is: "+ result1.value);
			cellLeft = result1.value.replace(/[^0-9]/g, '')
			//console.log("Left of cell is: "+ cellLeft);
			self.api.getCssProperty(destCellElementSelector, 'width', function(result3) {
				//console.log("Cell Width is: "+ result3.value);
				cellRight = Number(result3.value.replace(/[^0-9]/g, '')) + Number(cellLeft.replace(/[^0-9]/g, ''))
				//console.log("Right of cell is at: "+cellRight)  
				resolve(cellRight)
			})
		})
	});

	async function execute()	{
		Promise.all([promise1, promise2,promise3,promise4]).then(function(values) {
			var cellTop= values[0];
			//console.log("CELL TOP: "+cellTop);
			var cellBottom = values[1];
			//console.log("CELL BOTTOM: "+cellBottom);
			var cellLeft = values[2];
			//console.log("CELL LEFT: "+cellLeft);
			var cellRight = values[3];
			//console.log("CELL RIGHT: "+cellRight)

			var topborderOffset = Number(cellTop)-1
			//console.log("BORDER TOP: "+topborderOffset)
			var bottomborderOffset = cellBottom
			//console.log("BORDER BOTTOM: "+bottomborderOffset)
			var leftborderOffset = cellLeft-1
			//console.log("BORDER LEFT: "+leftborderOffset)
			var rightborderOffset = cellRight
			//console.log("BORDER RIGHT: "+rightborderOffset)

			var hBorderWidth = rightborderOffset - leftborderOffset;
			//console.log("WIDTH OF HORIZONTAL BORDER IS: "+ hBorderWidth)
			var vBorderHeight = bottomborderOffset - topborderOffset;
			vBorderHeight = vBorderHeight+1;
			//console.log("HEIGHT OF VERTICAL BORDER IS: "+ vBorderHeight)

			var topborderxpath = "//*[contains(@class,'k-spreadsheet-hborder') and contains(@style,'top: "+topborderOffset+"px; left: "+leftborderOffset+"px; width: "+hBorderWidth+"')]"
			//console.log("XPATH FOR TOP BORDER: "+topborderxpath)
			var bottomborderxpath = "//*[contains(@class,'k-spreadsheet-hborder') and contains(@style,'top: "+bottomborderOffset+"px; left: "+leftborderOffset+"px; width: "+hBorderWidth+"')]"
			//console.log("XPATH FOR BOTTOM BORDER: "+bottomborderxpath)
			var leftborderxpath = "//*[contains(@class,'k-spreadsheet-vborder') and contains(@style,'left: "+leftborderOffset+"px; top: "+topborderOffset+"px; height: "+vBorderHeight+"')]"
			//console.log("XPATH FOR LEFT BORDER: "+leftborderxpath)
			var rightborderxpath = "//*[contains(@class,'k-spreadsheet-vborder') and contains(@style,'left: "+rightborderOffset+"px; top: "+topborderOffset+"px; height: "+vBorderHeight+"')]"
			//console.log("XPATH FOR RIGHT BORDER: "+rightborderxpath)

			if (target == "Top Border") {
				self.api.element("xpath", topborderxpath, function(result) {
					//console.log("validating the presence of div")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for top border")
						validateProperty(topborderxpath,'top','1px')
					}
					else
						self.api.verify.equal(-1,0,"ERROR!! Top Border element DOES NOT EXIST: " + topborderxpath)
						if (typeof (cb) === "function") {
							cb.call(self.client.api);
							self.emit('complete');
							return this;
						}
				})
			}
			else if (target == "Bottom Border" || target == "Thick Bottom Border") {
				if (target == "Thick Bottom Border") {
					var expectedBorWidth = "2px"
					//console.log("If Block:Expected Border Width is: "+expectedBorWidth)
				}
				else {
					var expectedBorWidth = "1px"
					//console.log("Else block: Expected Border Width is: "+expectedBorWidth)
				}
				self.api.element("xpath", bottomborderxpath, function(result) {
					//console.log("validating the presence of div")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					//console.log("value found is"+result.value)
					if (0 == elementexists) {
						console.log("Element selector exists for bottom border")
						validateProperty(bottomborderxpath,'top',expectedBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Bottom Border element DOES NOT EXIST: " + bottomborderxpath)
					if (typeof (cb) === "function") {
						cb.call(self.client.api);
						self.emit('complete');
						return this;
					}
				})
			}
			else if (target == "Left Border") {
				self.api.element("xpath", leftborderxpath, function(result) {
					//console.log("validating the presence of div")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					//console.log("value found is"+result.value)
					if(0==elementexists) {
						console.log("Element selector exists for left border")
						validateProperty(leftborderxpath,'left','1px')
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Left Border element DOES NOT EXIST: " + leftborderxpath)
					if (typeof (cb) === "function") {
						cb.call(self.client.api);
						self.emit('complete');
						return this;
					}
				})
			}
			else if (target == "Right Border") {
				self.api.element("xpath", rightborderxpath, function(result) {
					//console.log("validating the presence of div")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					//console.log("value found is"+result.value)
					if (0 == elementexists) {
						console.log("Element selector exists for right border")
						validateProperty(rightborderxpath,'left','1px')
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Right Border element DOES NOT EXIST: " + rightborderxpath)
					if (typeof (cb) === "function") {
						cb.call(self.client.api);
						self.emit('complete');
						return this;
					}
				})
			}
			else if (target == "All Borders" || target == "Thick Outside Borders" || target == "Outside Borders") {
				if (target == "Thick Outside Borders") {
					var expectedBorWidth = "2px"
					//console.log("If Block:Expected Border Width is: "+expectedBorWidth)
				}
				else {
					var expectedBorWidth = "1px"
					//console.log("Else block: Expected Border Width is: "+expectedBorWidth)
				}
				self.api.element("xpath", topborderxpath, function(result) {
					//console.log("validating the presence of top border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for TOP border")
						validateProperty(topborderxpath,'top',expectedBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Top Border element DOES NOT EXIST: " + topborderxpath)
				})
				self.api.element("xpath", bottomborderxpath, function(result) {
					//console.log("validating the presence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for BOTTOM border")
						validateProperty(bottomborderxpath,'top',expectedBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Bottom Border element DOES NOT EXIST: " + bottomborderxpath)
				})
				self.api.element("xpath", leftborderxpath, function(result) {
					//console.log("validating the presence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for LEFT border")
						validateProperty(leftborderxpath,'left',expectedBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Left Border element DOES NOT EXIST: " + leftborderxpath)
				})
				self.api.element("xpath", rightborderxpath, function(result) {
					//console.log("validating the presence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for RIGHT border")
						validateProperty(rightborderxpath,'left',expectedBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Right Border element DOES NOT EXIST: " + rightborderxpath)
				})
				if (typeof (cb) === "function") {
					cb.call(self.client.api);
					self.emit('complete');
					return this;
				}
			}
			else if (target=="Top and Bottom Border" || target=="Top and Thick Bottom Border") {
				if (target=="Top and Thick Bottom Border") {
					var expectedBottomBorWidth = '2px'
					//console.log("IF Block:Expected Bottom border width is:"+expectedBottomBorWidth)
				}
				else {
					var expectedBottomBorWidth = '1px'
					//console.log("ELSE Block:Expected Bottom border width is:"+expectedBottomBorWidth)
				} 

				self.api.element("xpath", topborderxpath, function(result) {
					//console.log("validating the presence of top border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for TOP border")
						validateProperty(topborderxpath,'top','1px')
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Top Border element DOES NOT EXIST: " + topborderxpath)
				})
				self.api.element("xpath", bottomborderxpath, function(result) {
					//console.log("validating the presence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (0 == elementexists) {
						console.log("Element selector exists for BOTTOM border")
						validateProperty(bottomborderxpath,'top',expectedBottomBorWidth)
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Bottom Border element DOES NOT EXIST: " + bottomborderxpath)
				})
				if (typeof (cb) === "function") {
					cb.call(self.client.api);
					self.emit('complete');
					return this;
				}
			}
			else if (target == "No Border") {
				self.api.element("xpath", topborderxpath, function(result) {
					//console.log("validating the absence of top border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (-1 == elementexists) {
						console.log("Top Border does not exist for the cell/range")
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Top Border exist for this cell/range")
				})
				self.api.element("xpath", bottomborderxpath, function(result) {
					//console.log("validating the absence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (-1 == elementexists) {
						console.log("Bottom Border does not exist for the cell/range")
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Bottom Border exist for this cell/range")
				})
				self.api.element("xpath", leftborderxpath, function(result) {
					//console.log("validating the absence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (-1 == elementexists) {
						console.log("Left Border does not exist for the cell/range")
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Bottom Border exist for this cell/range")
				})
				self.api.element("xpath", rightborderxpath, function(result) {
					//console.log("validating the absence of bottom border")
					var elementexists = result.status
					//console.log("status got for element is"+elementexists)
					if (-1 == elementexists) {
						console.log("Right Border does not exist for the cell/range")
					}
					else 
						self.api.verify.equal(-1,0,"ERROR!! Right Border exist for this cell/range")
				})
				if (typeof (cb) === "function") {
					cb.call(self.client.api);
					self.emit('complete');
					return this;
				}
			}
			else {
				self.api.verify.equal(-1,0,"ERROR!! Invalid target (border type) " + target)
				console.log("ERROR!! Invalid target (border type) " + target)
			}
		});
	}
	
	function validateProperty(borderxpath,borType,expectedBorWidth)	{   
		var propertyTovalidate = "border-"+borType+"-width"
		//console.log("Border type is: "+borType)
		//console.log("Property to validate is" +propertyTovalidate)
		self.api.getCssProperty("xpath",borderxpath, propertyTovalidate, function(result1) {
			actualBorWidth = result1.value
			console.log("Expected Width of Border is:"+ expectedBorWidth);
			console.log("Actual width of border div is: "+ actualBorWidth);
			self.api.verify.equal(actualBorWidth, expectedBorWidth,"ERROR!! Actual and expected border width does not match")
		})
	}
}


module.exports = validateBorder;