var util = require('util');
var events = require('events');
var fs = require('fs');

function selectColumns() {

    events.EventEmitter.call(this);
}

util.inherits(selectColumns, events.EventEmitter);

selectColumns.prototype.command = function(range, cb) {

    var cellRange = String(range).split(":");
    var self = this;
	var startcol = "//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-column-header')]//*[text()='"+cellRange[0]+"']/.."
	var endcol = "//*[contains(@class,'leo-canvasarea')]//*[contains(@class,'k-spreadsheet-column-header')]//*[text()='"+cellRange[1]+"']/.."
	self.api.moveToElement("xpath", startcol, undefined, undefined)
            .mouseButtonDown(0)
			.pause(200)
            .moveToElement("xpath", endcol, undefined, undefined)
            .mouseButtonUp(0, function() {
                if (typeof(cb) === "function") {
                    cb.call(self.client.api);
                }
                self.emit('complete');
				return this;
    })
};

module.exports = selectColumns;