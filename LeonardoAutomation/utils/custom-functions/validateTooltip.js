var util = require('util');
var events = require('events');
var cssToXPath = require('css-to-xpath');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function validateTooltip() {
    events.EventEmitter.call(this);
}

util.inherits(validateTooltip, events.EventEmitter);

validateTooltip.prototype.command =  function(elementName,expected, cb) {
	var self = this;

   // var self = this;
	var elementSelector_control;
    var elementSelector_tooltip;
	var elementSelector_tooltipHeaderText;
	var elementSelector_tooltipDisplay;
	var elementSelectorExists = null;
	
	self.api.element("css selector",properties.get(elementName), function(result) {
		if (result.status ==-1)
		{
				console.log("ERROR!! element does not exist in props.properties "+ elementName);
				self.api.verify.equal(-1, 0, "ERROR!! Element does not exist in props.properties: "+ elementName);
				if (typeof(cb) === "function") {
					cb.call(self.client.api);	
				}
				self.emit('complete');
				return this;
		}
		else {
			elementSelector_control = cssToXPath(properties.get(elementName));	
			elementSelector_tooltip = elementSelector_control+ "//*[contains(@class,'lr_tooltip')]";
			elementSelector_tooltipDisplay = elementSelector_control+ "//*[contains(@class,'lr_tooltip') and contains(@style,'display: block')]";
			var promise1 = new Promise(function(resolve, reject) {
				self.api.element("xpath", elementSelector_tooltip, function(result) {		
					elementSelectorExists = result.status;	
					if (-1 == elementSelectorExists) {
						elementSelectorExists = null;
						elementSelector_tooltip = elementSelector_control+"//ancestor::*[contains(@class,'lr_tooltipWrapper')]//*[contains(@class,'lr_tooltip')]";
						self.api.element("xpath", elementSelector_tooltip, function(result1) {				
							elementSelectorExists = result1.status;
							elementSelector_tooltipDisplay = elementSelector_control+ "//ancestor::*[contains(@class,'lr_tooltipWrapper')]//*[contains(@class,'lr_tooltip') and contains(@style,'display: block')]";							
							resolve("done");			
						});
					}
					else {	
						resolve("done");	
					}
				});		
			});
		
			promise1.then(function() {	
				//console.log("inside promise then"+ elementSelectorExists + elementName);	
					
				if (0 == elementSelectorExists)	{				
					//xpath for tooltip header text
					elementSelector_tooltipHeaderText = elementSelector_tooltip+"//*[contains(@class,'lr_text')]";
					// move to element
					self.api.moveToElement("xpath",elementSelector_control,1,1,function() {
						// wait for tooltip to display
							self.api.useXpath(function(){
								self.api.waitForElementVisible(elementSelector_tooltipDisplay, 5000, 200, true, function(){}, "!!ERROR: Tooltip was not visible within 5 secs.");
								self.api.useCss();
							})
							// get the innertext of Tooltip Header Text if tooltip is visible
							self.api.getAttribute('xpath',elementSelector_tooltipHeaderText, "innerText", function(result) {
										console.log("Validating tooltip text of " + elementName);
										console.log("Tooltip Text Found: " + result.value);
										console.log("Tooltip Text Expected: " + expected);
										self.api.verify.equal(result.value, expected); 
										if (typeof(cb) === "function") {
											cb.call(self.client.api);	
										}
										self.emit('complete');
										return this;
									});
					});
				}
				else {
					//console.log("elementSelectorExists: "+ elementSelectorExists);
					console.log("ERROR!! Tootltip not found for element: "+ elementName);
					self.api.verify.equal(-1, 0, "ERROR!! Tootltip not found with the provided selector for: "+ elementName);
					if (typeof(cb) === "function") {
						cb.call(self.client.api);	
					}
					self.emit('complete');
					return this;
				}
			})
		}
	});
};

module.exports = validateTooltip;