var util = require('util');
var events = require('events');
var fs = require('fs');

var calculateDimensions = require('./../modules/getDimensions');


function selectRange() {

    events.EventEmitter.call(this);
}


util.inherits(selectRange, events.EventEmitter);

selectRange.prototype.command = function(range, cb) {

    var dragtarget = [];
    var droptarget = [];
    var offset = [];
    var cellRange = String(range).split(":");

    var self = this;

	function offsets(cellName) {
		var obj = calculateDimensions.getRowColRef(cellName)
		var rowNum = obj.row
		var colNum = obj.col
		
		return new Promise(async function(resolve, reject) {
			await calculateDimensions.getColWidthOffset(colNum, self).then(function(result) {
				offset[0] = result;
				//console.log("offset[0] " +offset[0]);
			})
			await calculateDimensions.getRowHeightOffset(rowNum, self).then(function(result1) {
				offset[1] = result1;
				//console.log("offset[1] " +offset[1]);
			})
			resolve(offset);
		})
	}

    var promise1 = new Promise(function(resolve, reject) {
        offsets(cellRange[0]).then(function(result) {
			//console.log("value " +result);
            resolve(result);
        });
    });

    var promise2 = new Promise(function(resolve, reject) {
		self.api.pause(500)
        offsets(cellRange[1]).then(function(result) {
			//console.log("value " +result);
            resolve(result);
        });
    });

    async function runSerial() {
        var draggablesrc = [];
        var draggableto = [];
		
		const executePromise1 = await promise1;
        //await (draggablesrc[0] = executePromise1[0]);
        //await (draggablesrc[1] = executePromise1[1]);
		draggablesrc[0] = executePromise1[0];
        draggablesrc[1] = executePromise1[1];

        const executePromise2 = await promise2;
        //await (draggableto[0] = executePromise2[0]);
        //await (draggableto[1] = executePromise2[1]);
		draggableto[0] = executePromise2[0];
        draggableto[1] = executePromise2[1];
		
		/*console.log("draggablesrc[0] " +draggablesrc[0]);
		console.log("draggablesrc[1] " +draggablesrc[1]);
		console.log("draggableto[0] " +draggableto[0]);
		console.log("draggableto[1] " +draggableto[1]);*/

	

	
		self.api.moveToElement("body", parseInt(draggablesrc[0]), parseInt(draggablesrc[1]))
				.mouseButtonDown(0)
				.pause(200)
				.moveToElement("body", parseInt(draggableto[0]), parseInt(draggableto[1]))
				.mouseButtonUp(0, function() {
					if (typeof(cb) === "function") {
						cb.call(self.client.api);
					}
					self.emit('complete');
				})
		return this;
    }
	
    runSerial();

}

module.exports = selectRange;