var util = require('util');
var events = require('events');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function movetoAnElement() {
    events.EventEmitter.call(this);
}


util.inherits(movetoAnElement, events.EventEmitter);

movetoAnElement.prototype.command = function (ElementName, x, y, preEvaluation, callback) {
    var self = this; 
	var promise2 = new Promise(function(resolve,reject)	{
		x_offset= calc_x_percent(self,properties.get(ElementName),x);
		y_offset= calc_y_percent(self,properties.get(ElementName),y);
		Promise.all([x_offset,y_offset]).then(function(values) {
			resolve(values);
		});
	});

	var promise1 = new Promise(function(resolve, reject) {
		self.api.getLocationInView(properties.get(ElementName),function (result)	{
			if(typeof result !== 'undefined')
			resolve(result);
		})
	});

	async function execute()	{
		Promise.all([promise1, promise2]).then(function(values) {
			coordinateorigin=values[0];
			offset = values[1];
			self.api.moveToElement("body", parseInt(coordinateorigin.value.x+offset[0]), parseInt(coordinateorigin.value.y+offset[1]))
			.pause(1000, function (result) {
				if (typeof (callback) === "function") {
					callback.call(self.client.api);
					self.emit('complete');
				}
			})
		});
		return this;
	}
	
	function calc_x_percent(self,element,value)	{   
		return new Promise(function(resolve,reject)	{
			var patt = /\%/;
			if (patt.test(value)) {
				value = value.split("%")[0];
				value = Number(value);
				self.api.getElementSize(element,function(result)	{ 
					resolve((result.value.width/100)*value);
				});
			}
			else	{
			  resolve(Number(value));
			}
		})
	}

	function calc_y_percent(self,element,value)	{
		return new Promise(function(resolve,reject)	{
			var patt = /\%/;
			if (patt.test(value)) {
				value = value.split("%")[0];
				value = Number(value);
				self.api.getElementSize(element,function(result)	{ 
					resolve((result.value.height/100)*value);
				});
			}
			else	{
			  resolve(Number(value));
			}
		})		
	}
	
	execute();
}


module.exports = movetoAnElement;