var util = require('util');
var events = require('events');
var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

function defaultFunctions() {
    events.EventEmitter.call(this);
}

util.inherits(defaultFunctions, events.EventEmitter);

defaultFunctions.prototype.command = function(event,target,cb) {

  var self = this;
  var fun = String(event)
  if(target=="" || target==undefined)
    {
      console.log("no parameter has been passed")
      var nightwatchFunCall = "this.api."+fun+"(function() {cb.call(self.client.api); self.emit('complete');return this;})"
      console.log("function called is: "+nightwatchFunCall)
      eval(nightwatchFunCall)
    }
  else
    {
      console.log("one parameter has been passed")
      var parameters = String(target)
      parameters = replaceString(parameters)
      var nightwatchFunCall = "self.api."+fun+"("+parameters+",function() {cb.call(self.client.api); self.emit('complete');return this;})"
      console.log("function called is: "+nightwatchFunCall)
      eval(nightwatchFunCall)
    }
};
function replaceString(parameters)
{
  if(parameters.includes("@"))
  {
    var substr1 = String(parameters).split("@");
    var index2 = substr1[1].indexOf("'")
    var Elementlength = index2
    var name = substr1[1].substr(0,Elementlength)
    console.log("Element Name is:"+name)
    var propcss = properties.get(name)
    var resparameters = parameters.replace("@"+name,propcss)
  }
  else
  {
    var resparameters=parameters
  }
 return resparameters;
};
  module.exports = defaultFunctions;
