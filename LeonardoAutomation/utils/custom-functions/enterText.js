var util = require('util');
var events = require('events');

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader("./utils/props.properties");

// var elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";

//div.k-spreadsheet-cell.k-spreadsheet-active-cell
function enterText() {
    events.EventEmitter.call(this);
}


util.inherits(enterText, events.EventEmitter);


enterText.prototype.command = function(text, elementName, cb) {

    var self = this;
    var elementSelector;
    //console.log(text);
    //self.api.url(function(result) {
        
        if (!elementName) {
            //self.api.doubleClick();
			//if (result.value.includes('embedframe') || result.value.includes('demos')) {
				elementSelector = "div.leo-canvasarea div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";
			/*} 
			else {
				elementSelector = "div.k-spreadsheet-cell-editor.k-spreadsheet-formula-input";
			}*/
			
			self.api.doubleClick();
			self.api.setValue(elementSelector, String(text), function() {
				self.api.pause(100);
				if (typeof(cb) === "function") {
					cb.call(self.client.api);
				}
				self.emit('complete');
				return this;
			});
        }
		
		else {
			self.api.setValue(properties.get(elementName), String(text), function() {
				self.api.pause(100);
				if (typeof(cb) === "function") {
					cb.call(self.client.api);
				}
				self.emit('complete');
				return this;
			});
		}
    //})
};

module.exports = enterText;