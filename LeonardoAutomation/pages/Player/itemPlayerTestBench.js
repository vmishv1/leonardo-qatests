module.exports = {
	
	elements: {
		
		name: {
			selector: '//input[@list = "datalist-name"]',
			locateStrategy: 'xpath'
		},

		version: {
			selector: '//input[@list = "datalist-version"]',
			locateStrategy: 'xpath'
		},

		uiOverrides: {
			selector: 'textarea#uiStyle'
		},

		containerWidth: {
			selector: 'input#width'
		},

		containerHeight: {
			selector: 'input#height'
		},

		applyButton: {
			selector: 'button.submitButton'
		}

	},

	commands: [
	
	{
		setName: function(name) {
			this.api.useXpath().setValue('//input[@list = "datalist-name"]', name).click('//label[text()="Name"]').useCss();
			return this
		},

		setVersion: function(version) {
			var temp = this
			if (version == "" || version == undefined || version == "latest") {
				this.api
					.waitForElementPresent('#datalist-version option:last-child', 10000)
					.getValue('#datalist-version option:last-child', function (result) {
						temp.api
							.useXpath()
							.setValue('//input[@list = "datalist-version"]', result.value).useCss();
					})
			} else {
				this.api.useXpath().setValue('//input[@list = "datalist-version"]', version).useCss();
			}
			return this
		},

		setContainerWidth: function(width) {
			this.api.clearValue('input#width').setValue('input#width', width)
			return this
		},

		setContainerHeight: function(height) {
			this.api.clearValue('input#height').setValue('input#height', height)
			return this
		},

		// setUiOverrides: function(height) {
		// 	this.api.setValue('@containerHeight', height)
		// 	return this
		// },

		setItemJSON: function(path) {
			var jsonData = require(currentDirPath + path)
			this.api
				.execute(function(json1) {
					document.getElementById('itemJson').value = JSON.stringify(json1)
				}, [jsonData])
				.pause(200).setValue('textarea#itemJson', " ")
				return this
				
		},

		applyConfigParams: function(config) {
			this
				.setName(config.name)
				.setVersion(config.version)
				.setItemJSON(config.itemJSON)
				.setContainerWidth(config.containerSettings.width)
				.setContainerHeight(config.containerSettings.height)
				.click('button.submitButton')
				.waitForElementVisible('div.l-act-player', 20000,"Element %s did not load in %d ms.")
			return this
		}
	}]

}
