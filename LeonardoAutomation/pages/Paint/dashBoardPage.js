module.exports = {

  elements: {
        
    publishItemNavigation:
      {
        selector: '.published-items'
      },
    previewActiveCell:
      {
        selector: '.k-spreadsheet-active-cell'
      },
    previewActiveCellData:
      {
        selector: '.k-spreadsheet-active-cell>.k-vertical-align-bottom'
      },

      previewQuestionActiveCell:
      {
        selector: '.leo-canvasarea .k-spreadsheet-active-cell'
      },
    previewQuestionActiveCellData:
      {
        selector: '.leo-canvasarea .k-spreadsheet-active-cell>.k-vertical-align-bottom'
    },
    searchBox:
    {
      selector: ".search-container .searchbox-input"
    },
    searchIcon:
    {
      selector: ".search-container .searchboxIcon"
    },
    sortDropDown: {
      selector: "#filter-type > i"
    }
  },

  commands: [
      
    { 
      
      openItemPreview: function(itemId) {
        try {
          this.api
            .waitForElementVisible('.published-items', 20000)
            .click('.published-items')
            .waitForElementPresent('.page-spinner-loader.hide', 20000)
            .useXpath()
            .moveToElement('//span[contains(text(), "' + itemId + '")]', 50, 50)
            .click('//span[contains(text(), "' + itemId + '")]//following::a[@title="Launch Preview"][1]')
            .useCss()
            .waitForElementPresent('div.leonardo-player-container',20000)

        } catch (err) {
          logger.error(err.stack)
        }
        return this
      },

      cloneItemWithID: function (itemId) {
        try {
          this.api
            .waitForElementVisible('.published-items', 20000)
            .click('.published-items')
            .waitForElementPresent('.page-spinner-loader.hide', 20000)
            .useXpath()
            .moveToElement('//span[contains(text(), "' + itemId + '")]', 50, 50)
            .click('//span[contains(text(), "' + itemId + '")]//following::a[@title="Clone Item"][1]')
            .useCss()
            .waitForElementVisible('div[aria-hidden=\'false\'] button.btn.btn-primary.btn--primary.modal-footer-btn-0', 20000)
            .click('div[aria-hidden=\'false\'] button.btn.btn-primary.btn--primary.modal-footer-btn-0')
            .waitForElementVisible('a.step.step-0.active.active-step > span.step-name', 20000);

        } catch (err) {
          logger.error(err.stack)
        }
        return this
      },
      cloneFirstItemofSearchList: function (itemId) {
        try {
          this.api
            .waitForElementPresent('.page-spinner-loader.hide', 20000)
            .moveToElement('item-list li:nth-child(1)', 50, 50)
            .click('item-list li:nth-child(1) .btn-item-clone')
            .waitForElementVisible('div[aria-hidden=\'false\'] button.btn.btn-primary.btn--primary.modal-footer-btn-0', 20000)
            .click('div[aria-hidden=\'false\'] button.btn.btn-primary.btn--primary.modal-footer-btn-0')
            .waitForElementVisible('a.step.step-0.active.active-step > span.step-name', 20000);

        } catch (err) {
          logger.error(err.stack)
        }
        return this
      },

      openEmbed: function(itemId) { 
        try {
          this.api
            .waitForElementVisible('.published-items',20000)
            .click('.published-items')
            .waitForElementPresent('.page-spinner-loader.hide',20000)
            .useXpath()
            .moveToElement('//span[contains(text(), "' + itemId + '")]', 50, 50)
            .click('//span[contains(text(), "' + itemId + '")]//following::a[@title="Embed Item"][1]')
            .useCss()
            .waitForElementVisible('.modal-embed .modal-header',20000)
        } catch (err) {
          logger.error(err.stack)
        }
        return this
      },

      searchProject: function (Name) {
        try {
          logger.info("Serarch for Project " + Name)
          this
            .waitForElementVisible('@searchBox', 40000)
            .setValue('@searchBox', Name)
            .click('@searchIcon')
            .api.pause(10000)
          this.waitForElementNotPresent('filters-panel visible', 20000, function (result) {
            var test1 = this
            if (result.status === 0)
              logger.info("leonardo Paint : Search Successful")
            test1.getText('.itemsWrapper .items-found>b', function (result) {
              logger.info(result.value + " projects found with the same name")
              if (result.value === 0) {
                logger.error(result.value + " projects found with the same Name")
              }
            })
          })

        }
        catch (err) {
          logger.error(err.stack)
        }
        return this
      },
      sortList: function (Name) {
        try {
          logger.info("Sort for Project List ")
          this
            .waitForElementVisible('@sortDropDown', 40000)
            .click('@sortDropDown')
            .waitForElementVisible("div.filter-dropdown.btn-group a[title^='" + Name + "']", 20000)
            .click("div.filter-dropdown.btn-group a[title^='" + Name + "']")

        }
        catch (err) {
          logger.error(err.stack)
        }
        return this
      }

    }]
};
 