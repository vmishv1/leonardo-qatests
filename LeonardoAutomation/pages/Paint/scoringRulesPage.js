module.exports = {
	elements: {

		totalScoreInput: {
			selector: 'div.score-heading > input'
		},

		totalScoreInDescription: {
			selector: 'div.instruction2 > b:nth-child(1)'
		},

		tableRows: {
			selector: 'div.table-row'
		},

		feedbackTextArea: {
			selector: 'textarea.feedback-textarea'
		},

		hintInputArea: {
			selector: 'div.hint-input'
		},

		penaltyInputArea: {
			selector: 'input.penalty-input'
		},

		addNewRule: {
			selector: 'div.addRuleButton'
		},

		addNewCell: {
			selector: 'div.addRuleForm div.range input'
		},

		addNewScore: {
			selector: 'div.addRuleForm div.score input'
		},

		initialDocument: {
			selector: 'a.initial-doc'
		},

		finalDocument: {
			selector: 'a.final-doc'
		},
		mapperWarning: {
			selector: 'div:nth-child(5) > cell-handler a.ruleStatus'

		},
		modalConfirmation:
		{
			selector: '.modal-confirmation rule-map-summary  button'
		},
		hintSummary:
		{
			selector: '.toolbar-btn>i.fa.fa-lightbulb-o.mr-2'
		},
		settings:
		{
			selector: '.toolbar-btn>i.fa.fa-cog.mr-2'
		},
		saveSettings:
		{
			selector: '//button[text()="Save"]',
			locateStrategy: 'xpath'
		}

	},

	commands: [

		{

			toggleScore: function (rowNumber) {
				try {
					var temp = this
					this
						.api.elements('css selector', 'div.toggle-rule > i', function (result) {
							temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function () {
								temp
									.api.elements('css selector', 'div.table-row', function (result2) {
										temp.api.elementIdAttribute(result2.value[rowNumber - 1].ELEMENT, "class", function (res) {
											temp.verify.notEqual(res.value.indexOf("disable-row"), -1, 'Failed to toggle the score for the specified row')
										})
									})
							})
						})
				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},

			addFeedbackText: function (feedbackText, rowNumber) {
				var temp = this
				try {
					this
						.api.elements('xpath', '//a[@title="Feedback"]', function (result) {
							temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function () {
								temp
									.waitForElementVisible('@feedbackTextArea', 10000)
									.click('@feedbackTextArea')
									.clearValue('@feedbackTextArea')
									.setValue('@feedbackTextArea', feedbackText)
									.api.useXpath()
									.click('//button[text()="Apply"]')
									.waitForElementPresent('//modal[contains(@class, "feedback")]//div[contains(@style, "display: none")]')
									.useCss()
							})
						})
				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},

			addHint: function (hintInputText, penaltyScore, rowNumber) {
				var temp = this
				try {
					this
						.api.elements('xpath', '//a[contains(@title, "Hint Available")]', function (result) {
							temp.api.elementIdClick(result.value[rowNumber - 1].ELEMENT, function () {
								temp
									.waitForElementVisible('@hintInputArea', 10000)
									.click('@hintInputArea')
									.clearValue('@hintInputArea')
									.setValue('@hintInputArea', hintInputText)
									.click('@penaltyInputArea')
									.clearValue('@penaltyInputArea')
									.setValue('@penaltyInputArea', penaltyScore)
									.api.useXpath()
									.click('//div[contains(@style, "display: block")]//button[text()="Done"]')
									.waitForElementPresent('//modal[contains(@class, "hint")]//div[contains(@style, "display: none")]')
									.useCss()
							})
						})
				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},
			addHintfromhintsummary: function (hintInputText, penaltyScore) {
				var temp = this
				logger.info("Add Hint from HintSummary")
				try {
					this

						.waitForElementVisible('@hintInputArea', 10000)
						.click('@hintInputArea')
						.clearValue('@hintInputArea')
						.setValue('@hintInputArea', hintInputText)
						.click('@penaltyInputArea')
						.clearValue('@penaltyInputArea')
						.setValue('@penaltyInputArea', penaltyScore)
						.api.useXpath()
						.click('//div[contains(@style, "display: block")]//button[text()="Done"]')
						.waitForElementPresent('//modal[contains(@class, "hint")]//div[contains(@style, "display: none")]')
						.useCss()


				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},
			deleteHintfromhintsummary: function (hintInputText, penaltyScore) {

				hintvalue = '//span[contains(@class, \'label\') and text() = \'Hint 2\']/ancestor::div[@class=\'hint-summary-header\']//div/span/a[@title=\'Delete\']'
				logger.info("Delete Hint from HintSummary")
				try {
					this
						.api.useXpath()
						.waitForElementVisible(hintvalue, 10000)
						.click(hintvalue)

						.useCss()
						.waitForElementVisible('.hint-summary-item a.actions-btn.delete', 10000)
						.click('.hint-summary-item a.actions-btn.delete')



				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},

			hintTypeSelection: function (type) {
				try {
					this
						.api.pause(2000)
						.useXpath()
						//	.waitForElementVisible('.//label[contains(text(),\''+type+'\')]/span', 20000)	
						.moveToElement('.//label[contains(text(),\'' + type + '\')]/span', 500, 500)
						.click('.//label[contains(text(),\'' + type + '\')]/span')
						.click('//button[text()="Save"]')
						.pause(2000)
						.useCss()
				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},

			addNewRule: function (cellValue, score) {
				try {
					this
						.waitForElementVisible('@addNewRule', 10000)
						.click('@addNewRule')
						.setValue('@addNewCell', cellValue)
						.click('@addNewScore')
						.clearValue('@addNewScore')
						.setValue('@addNewScore', score)
						.api.useXpath()
						.click('//a[@title="Save Rule"]')
						.useCss()
				} catch (err) {
					logger.error(err.stack)
				}
				return this
			},
			applyWarningMessage: function () {
				var temp = this
				try {
					this
						.click('@mapperWarning')
						.api.pause(2000)
					temp
						.click('@modalConfirmation')
						.api.pause(2000)

				} catch (err) {
					logger.error(err.stack)
				}
				return this

			}
		}]

};