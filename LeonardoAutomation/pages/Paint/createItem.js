module.exports = {

	elements : {
		create: {
			selector :'.btn-create-new>i'
		},
		title : {
			selector : '#title'
		},
		presetationItem : {
			selector :'.mode-container>div:nth-child(3)'	
		},
		questionItem : {
			selector : '.mode-container>div:nth-child(2)',
		},
		proceedButton : {
			selector : 'div[aria-hidden=\'false\'] button.btn.modal-footer-btn-0'
		},
		createWindow : {
			selector :'.new-leonardo-item>div[aria-hidden=\'false\']'
		},
		createPageHeader: {
			selector: '.new-leonardo-item .modal-header'
		}

},

	commands : [
	{
		openCreate: function() {
			try {
                logger.info("Click on Create a New Item")
				this
					.waitForElementVisible('@create',20000)
					.click('@create')
					.waitForElementVisible('@createWindow',20000)
			} catch (err) {
                logger.error(err.stack)
           	}
           	return this
		},
	
		createItem: function(title,item) {
			if (item.toLowerCase() == 'presentation')
				item = '@presetationItem'
			else 
				item = '@questionItem';	
			try {
                logger.info("Create a New Item")
				this
					.waitForElementVisible('@title' , 20000)
					.setValue('@title', title)
					.waitForElementVisible (item ,20000)
					.click(item)
					.waitForElementVisible ('@proceedButton',20000)
					.click('@proceedButton')
				logger.info("New Item is created")
			} catch (err) {
                logger.error(err.stack)
           	}
            return this
	 	}
	}]
};
