# Running Paint Tests

To run Paint tests, the *appType* parameter needs to defined first. For e.g.
```
npm test -- --appType=Paint
```

To run any/all of the Paint tests, use parameter **testFiles**. For e.g.
```
npm test -- --appType=Paint --testFiles="./tests/Paint"
```
Similarly, to run a specific testfile (say QuestionAcceptance.js file), following command can be passed:
```
npm test -- --appType=Paint --testFiles="./tests/Paint/Default/AcceptanceTest/QuestionAcceptance.js"
```

## Run Time Parameters available for Item Renderer Tests:

To run the Paint tests in a specific environment, use parameter **testEnv**. For e.g.

- **testEnv** - To run Paint tests in any environment(QA, Staging or Production), use parameter **testEnv**. For e.g. The below command will run all the testcases of the files present inside the Paint folder in the staging environment

```
npm test -- --appType=Paint --testFiles="./tests/Paint" --testEnv=staging
```
The testEnv parameter has following default values defined in the framework for the TestEnv parameter:
** QA
** Staging
** Production

Other than the above values, a **URL** can also be passed in the testEnv variable. It will launch that variable. Along with the URL following parameters need also to be defined to login into the system:
- **org** - Organization Name
- **username** - User Name
- **password** - Password Details

```
npm test -- --appType=paint --testFiles="./tests/Paint/Default/AcceptanceTest/questionAcceptanceTest.js" --testEnv="http://leonardo-paint-any-url.herokuapp.com/" --org=leonardo-dev --username=qa_author1 --password=ComproTest
```

### Other Parameters available:

- **grep** - To run any specific testcase, **grep** command can be used. For e.g. The below command will run all the testcases inside the "AcceptanceTest" folder which includes "TC02" in the test case name on the "qa" environment.

```
npm test -- --appType=Paint --testFiles="./tests/Paint/Default/AcceptanceTest" --testEnv=qa --grep=TC02
```

- **invert** - To run all testcases which does **NOT** include any specific word, use the **invert** parameter with **grep** command. For e.g. The below command will run all the testcases inside the "AcceptanceTest" folder which does not include "TC02" in the test case name on the "qa" environment.

```
npm test -- --appType=Paint --testFiles="./tests/Paint/Default/AcceptanceTest" --testEnv=qa --grep=TC02 --invert
```

## Default Run Commands already added in Package.json
For ease, following default commands are already present inside the package.json file
```
npm run paintQA 
	--Run all testfiles present inside Paint folder in QA environment 
npm run paintStg
	--Run all testfiles present inside Paint folder in Staging environment 
npm run paintProd
	--Run all testfiles present inside Paint folder in Production environment 
```